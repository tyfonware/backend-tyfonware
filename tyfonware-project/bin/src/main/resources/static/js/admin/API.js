class API {
     async obtenerCoordenadas() {
          const data = await fetch('/admin/casas/condominioCoordenadas')
          const json = await data.json();
          return json;
     }
     async obtenerCasas() {
         const data = await fetch('/admin/casas/casasRest')
         const json = await data.json();
         return json;
    }
    async obtenerDepositos() {
         const data = await fetch('/admin/rest/residuos/recoleccion')
         const json = await data.json();
         return json;
    }
}