INSERT INTO `users` (username, password, enabled) VALUES ('admin','$2a$10$DOMDxjYyfZ/e7RcBfUpzqeaCs8pLgcizuiQWXPkU35nOhZlFcE9MS',1);

INSERT INTO `authorities` (user_id, authority) VALUES (1,'ROLE_ADMIN');
DELIMITER $$
CREATE TRIGGER elimina_relacion_casa_usuarios BEFORE DELETE ON casas
  FOR EACH ROW BEGIN
  	DELETE FROM propietarios_casas WHERE id_casa = OLD.id;
  END$$
  
 DELIMITER ;
  
  INSERT INTO condominios (latitud,longitud,nombre,saldo,id_administrador) values (19.3133718,-99.1148944,"Los Cedros",0,1);
  
  insert into tipos_residuo(nombre) values ('prueba1');
  insert into tipos_residuo(nombre) values ('prueba2');
  insert into tipos_residuo(nombre) values ('prueba3');
  
  insert into tipos_residuo(nombre) values ('prueba3'); 
 insert into residuos(informacion,nombre,precio,tipo_residuo_id) values('probando...','test2',200,1);
 insert into residuos(informacion,nombre,precio,tipo_residuo_id) values('probando...','test3',300,2);
 insert into residuos(informacion,nombre,precio,tipo_residuo_id) values('probando...','test4',400,2);
 insert into residuos(informacion,nombre,precio,tipo_residuo_id) values('probando...','test5',500,3);