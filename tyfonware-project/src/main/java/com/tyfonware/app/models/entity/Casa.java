package com.tyfonware.app.models.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name = "casas")
public class Casa implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotEmpty
	private String direccion;

	@NotNull
	private Double latitud;

	@NotNull
	private Double longitud;

	// Propietarios
	
	@JsonBackReference
	@ManyToMany(mappedBy = "casas", fetch = FetchType.LAZY)
	private List<Propietario> propietarios;
	
	//Condominio
	@JsonBackReference
	@ManyToOne(fetch = FetchType.LAZY)
	private Condominio condominio;
	
	@JsonBackReference
	@OneToMany(mappedBy = "casa", fetch = FetchType.LAZY,cascade = CascadeType.ALL)
	private List<DepositoBasura> depositosBasura;
	
	@JsonBackReference
	@OneToMany(mappedBy = "casa", fetch = FetchType.LAZY,cascade = CascadeType.ALL)
	private List<Pago> pagos;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public Double getLatitud() {
		return latitud;
	}

	public void setLatitud(Double latitud) {
		this.latitud = latitud;
	}

	public Double getLongitud() {
		return longitud;
	}

	public void setLongitud(Double longitud) {
		this.longitud = longitud;
	}

	public List<Propietario> getPropietarios() {
		return propietarios;
	}

	public void setPropietarios(List<Propietario> propietarios) {
		this.propietarios = propietarios;
	}

	public Condominio getCondominio() {
		return condominio;
	}

	public void setCondominio(Condominio condominio) {
		this.condominio = condominio;
	}

	public List<DepositoBasura> getDepositosBasura() {
		return depositosBasura;
	}

	public void setDepositosBasura(List<DepositoBasura> depositosBasura) {
		this.depositosBasura = depositosBasura;
	}

	public List<Pago> getPagos() {
		return pagos;
	}

	public void setPagos(List<Pago> pagos) {
		this.pagos = pagos;
	}
	
	

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	
}
