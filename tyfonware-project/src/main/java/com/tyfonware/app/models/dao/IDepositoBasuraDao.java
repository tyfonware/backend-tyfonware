package com.tyfonware.app.models.dao;

import java.sql.Date;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.tyfonware.app.models.entity.DepositoBasura;;

public interface IDepositoBasuraDao extends CrudRepository<DepositoBasura, Long>{
	
	@Query("select d from DepositoBasura d join fetch d.casa c join fetch d.recoleccion r where c.id = :casaID and r.createAt = :date")
	public DepositoBasura findByCasaAndDate(@Param("casaID")Long casaID, @Param("date")Date date);

}
