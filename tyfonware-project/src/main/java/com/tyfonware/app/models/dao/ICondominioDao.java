package com.tyfonware.app.models.dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.tyfonware.app.models.entity.Condominio;

public interface ICondominioDao extends CrudRepository<Condominio, Long>{
	
	@Query("select c from Condominio c join fetch c.administrador a where a.username = :usuario")
	public Condominio findCondominioByAdministrador(@Param("usuario")String username);
	
}
