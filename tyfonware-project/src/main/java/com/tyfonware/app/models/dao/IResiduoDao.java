package com.tyfonware.app.models.dao;

import org.springframework.data.repository.CrudRepository;

import com.tyfonware.app.models.entity.Residuo;

public interface IResiduoDao extends CrudRepository<Residuo,Long>{
	
	
	
}
