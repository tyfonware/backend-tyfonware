package com.tyfonware.app.models.dao;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.tyfonware.app.models.entity.Casa;
import com.tyfonware.app.models.entity.Condominio;

public interface ICasaDao extends PagingAndSortingRepository<Casa,Long>{
	
	public List<Casa> findByDireccionLikeIgnoreCase(String term);
	
	public List<Casa> findByCondominioAndDireccionLikeIgnoreCase(Condominio condominio,String term);
	
	public List<Casa> findByCondominio(Condominio condominio);
	
	public Page<Casa> findByCondominio(Condominio condominio,Pageable pageable);
	
	public Page<Casa> findByCondominioAndDireccionLikeIgnoreCase(Condominio condominio, String term,Pageable pageable);
	
}
