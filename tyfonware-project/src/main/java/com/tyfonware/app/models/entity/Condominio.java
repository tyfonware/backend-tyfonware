package com.tyfonware.app.models.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;

@Entity
@Table(name = "condominios")
public class Condominio implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotEmpty
	private String nombre;

	@NotNull
	private Double latitud;

	@NotNull
	private Double longitud;

	@NotNull
	private Double saldo;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_administrador")
	private Usuario administrador;

	@JsonBackReference
	@OneToMany(mappedBy = "condominio", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<Gasto> gastos;

	@OneToMany(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_condominio")
	private List<TipoPago> tipoPago;

	@JsonManagedReference
	@OneToMany(mappedBy = "condominio", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<Casa> casas;

	@JsonBackReference
	@OneToMany(mappedBy = "condominio", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<Recoleccion> recolecciones;
	
	@JsonBackReference
	@OneToMany(mappedBy = "condominio", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<Ingreso> ingresos;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Double getLatitud() {
		return latitud;
	}

	public void setLatitud(Double latitud) {
		this.latitud = latitud;
	}

	public Double getLongitud() {
		return longitud;
	}

	public void setLongitud(Double longitud) {
		this.longitud = longitud;
	}

	public Double getSaldo() {
		return saldo;
	}

	public void setSaldo(Double saldo) {
		this.saldo = saldo;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public List<Gasto> getGastos() {
		return gastos;
	}

	public void setGastos(List<Gasto> gastos) {
		this.gastos = gastos;
	}

	public Usuario getAdministrador() {
		return administrador;
	}

	public void setAdministrador(Usuario administrador) {
		this.administrador = administrador;
	}

	public List<TipoPago> getTipoPago() {
		return tipoPago;
	}

	public void setTipoPago(List<TipoPago> tipoPago) {
		this.tipoPago = tipoPago;
	}

	public List<Casa> getCasas() {
		return casas;
	}

	public void setCasas(List<Casa> casas) {
		this.casas = casas;
	}

	public List<Recoleccion> getRecolecciones() {
		return recolecciones;
	}

	public void setRecolecciones(List<Recoleccion> recolecciones) {
		this.recolecciones = recolecciones;
	}
	
	
	
	public void enviarNotificacion(String mensaje, List<Propietario> propietarios) {

		final String ACCOUNT_SID = "AC32428006c524bffd6a2271524335b742";
		final String AUTH_TOKEN = "38a7f99bbcec1fdaeb9eaeb7a1a03193";

		Twilio.init(ACCOUNT_SID, AUTH_TOKEN);

		for (Propietario p : propietarios) {
			try {
				
				Message message = Message.creator(new com.twilio.type.PhoneNumber("whatsapp:+521" + p.getTelefono()),
						new com.twilio.type.PhoneNumber("whatsapp:+14155238886"),
						"Notificación de *" + this.getNombre() + "*: " + mensaje).create();
				System.out.println(message.getSid());
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}

		}

	}
	
	public Boolean modificarSaldo(Double monto) {
		Double saldoModificado = this.saldo+monto;
		if(saldoModificado<0) {
			return false;
		}
		this.saldo = saldoModificado;
		return true;
	}

}
