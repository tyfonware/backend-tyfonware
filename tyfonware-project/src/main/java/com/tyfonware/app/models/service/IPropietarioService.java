package com.tyfonware.app.models.service;

import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.query.Param;

import com.tyfonware.app.models.entity.Propietario;

public interface IPropietarioService {
	
	public List<Propietario> findAll();

	public void save(Propietario propietario);
	
	public Propietario findById(Long id);
	
	public void delete(Long id);
	
	public List<Propietario> findByIdCondominio(Long id);
	
	public Propietario findByUsernameUsuario(String username);
	
	public Page<Propietario> findAllByIdCondominio(Long id,Pageable pageable);
	public Page<Propietario> findAllByIdCondominio(@Param("idCondominio")Long id,@Param("term")String term,Pageable pageable);
	
	public Propietario findByEmailOrTelefono(String email, String telefono);

}
