package com.tyfonware.app.models.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tyfonware.app.models.dao.IPropietarioDao;
import com.tyfonware.app.models.entity.Propietario;

@Service
public class PropietarioServiceImp implements IPropietarioService {
	
	@Autowired
	private IPropietarioDao propietarioDao;
	
	@Override
	@Transactional(readOnly = true)
	public List<Propietario> findAll() {
		
		return (List<Propietario>) propietarioDao.findAll();
	}

	@Override
	@Transactional
	public void save(Propietario propietario) {
		
		propietarioDao.save(propietario);
		
	}

	@Override
	@Transactional(readOnly = true)
	public Propietario findById(Long id) {
		
		return propietarioDao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public void delete(Long id) {
		propietarioDao.deleteById(id);
		
	}

	@Override
	public List<Propietario> findByIdCondominio(Long id) {
		// TODO Auto-generated method stub
		return propietarioDao.findByIdCondominio(id);
	}

	@Override
	public Propietario findByUsernameUsuario(String username) {
		// TODO Auto-generated method stub
		return propietarioDao.findByUsernameUsuario(username);
	}

	@Override
	public Page<Propietario> findAllByIdCondominio(Long id,Pageable pageable) {
		// TODO Auto-generated method stub
		return propietarioDao.findAllByIdCondominio(id,pageable);
	}

	@Override
	public Page<Propietario> findAllByIdCondominio(Long id, String term, Pageable pageable) {
		// TODO Auto-generated method stub
		return propietarioDao.findAllByIdCondominio(id, term, pageable);
	}

	@Override
	public Propietario findByEmailOrTelefono(String email, String telefono) {
		// TODO Auto-generated method stub
		return propietarioDao.findByEmailOrTelefono(email, telefono);
	}

}
