package com.tyfonware.app.models.dao;

import org.springframework.data.repository.CrudRepository;

import com.tyfonware.app.models.entity.TipoResiduo;

public interface ITipoResiduoDao extends CrudRepository<TipoResiduo,Long>{
	
	
	
}
