package com.tyfonware.app.models.entity;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name="residuos")
public class Residuo implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@NotEmpty
	private String nombre;
	
	@NotEmpty
	private Double precio;
	
	@NotEmpty
	private String informacion;
	
	
	
	//Getters and setters
	@JsonBackReference
	@ManyToOne(fetch=FetchType.LAZY)
	private TipoResiduo tipoResiduo;
	
		
	public Long getId() {
		return id;
	}

	
	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public Double getPrecio() {
		return precio;
	}

	public void setPrecio(Double precio) {
		this.precio = precio;
	}
	
	public String getInformacion() {
		return informacion;
	}

	public void setInformtacion(String informacion) {
		this.informacion = informacion;
	}


	public TipoResiduo getTipoResiduo() {
		return tipoResiduo;
	}


	public void setTipoResiduo(TipoResiduo tipoResiduo) {
		this.tipoResiduo = tipoResiduo;
	}


	public void setInformacion(String informacion) {
		this.informacion = informacion;
	}


	public static long getSerialversionuid() {
		return serialVersionUID;
	}



	
	
	
	
}
