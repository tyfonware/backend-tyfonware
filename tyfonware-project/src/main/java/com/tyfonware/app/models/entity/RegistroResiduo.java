package com.tyfonware.app.models.entity;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;



@Entity
@Table(name="registros_residuos")
public class RegistroResiduo implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@NotNull
	private  Integer cantidad;
	
	
	private Double monto;
	
		
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "id_residuo")
	private Residuo residuo;
	
	public Long getId() {
		return id;
	}

	
	public void setId(Long id) {
		this.id = id;
	}

	public Integer getCantidad() {
		return cantidad;
	}


	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}


	public Residuo getResiduo() {
		return residuo;
	}


	public void setResiduo(Residuo residuo) {
		this.residuo = residuo;
	}


	public Double getMonto() {
		return monto;
	}

	public void setMonto(Double monto) {
		this.monto = monto;
	}
	
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
}
