package com.tyfonware.app.models.service;

import java.sql.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tyfonware.app.models.dao.IDepositoBasuraDao;
import com.tyfonware.app.models.dao.IRecoleccionDao;
import com.tyfonware.app.models.dao.IResiduoDao;
import com.tyfonware.app.models.dao.ITipoResiduoDao;
import com.tyfonware.app.models.entity.Condominio;
import com.tyfonware.app.models.entity.DepositoBasura;
import com.tyfonware.app.models.entity.Recoleccion;
import com.tyfonware.app.models.entity.Residuo;
import com.tyfonware.app.models.entity.TipoResiduo;

@Service
public class ResiduoServiceImp implements IResiduoService {
	
	@Autowired
	private IDepositoBasuraDao depositoBasuraDao;
	
	@Autowired
	private IRecoleccionDao recoleccionDao;
	
	@Autowired
	private ITipoResiduoDao tipoResiduoDao;
	
	@Autowired
	private IResiduoDao residuoDao;

	@Override
	@Transactional
	public void save(DepositoBasura deposito) {
		depositoBasuraDao.save(deposito);

	}
	
	@Override
	public DepositoBasura findByCasaAndDate(Long casaID, Date date) {
		// TODO Auto-generated method stub
		return depositoBasuraDao.findByCasaAndDate(casaID, date);
	}
	
	
	//Recoleccion
	
	@Override
	@Transactional
	public void save(Recoleccion recoleccion) {
		recoleccionDao.save(recoleccion);
		
	}

	@Override
	@Transactional(readOnly = true)
	public Recoleccion findByDate(Date date) {
		// TODO Auto-generated method stub
		return recoleccionDao.findByDate(date);
	}


	@Override
	public Recoleccion findByCondominioAndDate(Condominio con, Date date) {
		// TODO Auto-generated method stub
		return recoleccionDao.findByCondominioAndDate(con, date);
	}
	
	//Tipo Residuo
	
	@Override
	public List<TipoResiduo> findAllTipoResiduo() {
		// TODO Auto-generated method stub
		return (List<TipoResiduo>) tipoResiduoDao.findAll();
	}

	@Override
	public TipoResiduo findTipoResiduoById(Long id) {
		// TODO Auto-generated method stub
		return tipoResiduoDao.findById(id).orElse(null);
	}

	@Override
	public Residuo findResiduoById(Long id) {
		// TODO Auto-generated method stub
		return residuoDao.findById(id).orElse(null);
	}


	

}
