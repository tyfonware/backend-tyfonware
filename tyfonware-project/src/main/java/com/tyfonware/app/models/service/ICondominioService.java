package com.tyfonware.app.models.service;

import java.util.List;

import com.tyfonware.app.models.entity.Condominio;


public interface ICondominioService {

	public List<Condominio> findAll();

	public void save(Condominio condominio);
	
	public Condominio findById(Long id);
	
	public void delete(Long id);
	
	public Condominio findCondominioByAdministrador(String username);
}
