package com.tyfonware.app.models.service;

import java.util.List;

import com.tyfonware.app.models.entity.Usuario;

public interface IUsuarioService {
	
	public List<Usuario> findAll();

	public void save(Usuario usuario);
	
	public Usuario findById(Long id);
	
	public Usuario findByUsername(String username);
	
	public void delete(Long id);
	
}
