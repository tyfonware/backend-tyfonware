package com.tyfonware.app.models.dao;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.tyfonware.app.models.entity.Condominio;
import com.tyfonware.app.models.entity.Ingreso;

public interface IIngresoDao extends JpaRepository<Ingreso, Long>{
	
	public List<Ingreso> findByCondominio(Condominio condominio);
	
	@Query("select i from Ingreso i where i.condominio = :condominio order by i.id desc")
	public List<Ingreso> findByCondominioOrderByDate(@Param("condominio")Condominio condominio,Pageable pageable);
	
	@Query("select i from Ingreso i where i.condominio = :condominio order by i.id desc")
	public Page<Ingreso> findByCondominio(@Param("condominio")Condominio condominio,Pageable pageable);
 }
