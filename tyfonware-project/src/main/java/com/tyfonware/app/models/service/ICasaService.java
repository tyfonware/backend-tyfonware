package com.tyfonware.app.models.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.tyfonware.app.models.entity.Casa;
import com.tyfonware.app.models.entity.Condominio;


public interface ICasaService {

	public List<Casa> findAll();

	public void save(Casa casa);
	
	public Casa findById(Long id);
	
	public void delete(Long id);
	
	public List<Casa> findByDireccion(String term);
	
	public List<Casa> findByCondominio(Condominio condominio);
	
	public Page<Casa> findByCondominio(Condominio condominio,Pageable pageable);
	
	public List<Casa> findByCondominioAndDireccionLikeIgnoreCase(Condominio condominio,String term);
	
	public Page<Casa> findByCondominioAndDireccionLikeIgnoreCase(Condominio condominio, String term,Pageable pageable);

	
}
