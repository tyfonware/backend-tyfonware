package com.tyfonware.app.models.dao;


import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.tyfonware.app.models.entity.Condominio;
import com.tyfonware.app.models.entity.Gasto;

public interface IGastoDao extends JpaRepository<Gasto, Long>{
	
	public List<Gasto> findByCondominio(Condominio condominio);
	
	@Query("select g from Gasto g where g.condominio = :condominio order by g.id desc")
	public List<Gasto> findByCondominioOrderByDate(@Param("condominio")Condominio condominio,Pageable pageable);
	
	@Query("select g from Gasto g where g.condominio = :condominio order by g.id desc")
	public Page<Gasto> findByCondominio(@Param("condominio")Condominio condominio,Pageable pageable);
}
