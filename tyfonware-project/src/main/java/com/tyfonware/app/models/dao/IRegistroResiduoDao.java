package com.tyfonware.app.models.dao;

import org.springframework.data.repository.CrudRepository;

import com.tyfonware.app.models.entity.RegistroResiduo;

public interface IRegistroResiduoDao extends CrudRepository<RegistroResiduo, Long>{
	
}
