package com.tyfonware.app.models.dao;

import org.springframework.data.repository.CrudRepository;

import com.tyfonware.app.models.entity.Pago;

public interface IPagoDao extends CrudRepository<Pago, Long>{
	
}
