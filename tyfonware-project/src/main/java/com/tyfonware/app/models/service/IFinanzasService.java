package com.tyfonware.app.models.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.tyfonware.app.models.entity.Condominio;
import com.tyfonware.app.models.entity.Gasto;
import com.tyfonware.app.models.entity.Ingreso;
import com.tyfonware.app.models.entity.Pago;

public interface IFinanzasService {

	public void save(Ingreso ingreso);
	public List<Ingreso> findIngresoByCondominio(Condominio condominio);
	public Page<Ingreso> findIngresoByCondominio(Condominio condominio,Pageable pageable);
	public List<Ingreso> findIngresoByCondominioOrderByDate(Condominio condominio,Pageable pageable);
	public Ingreso findIngresoById(Long id);
	//Gastos
	
	public void save(Gasto gasto);
	public List<Gasto> findGastoByCondominio(Condominio condominio);
	public Page<Gasto> findGastoByCondominio(Condominio condominio,Pageable pageable);
	public List<Gasto> findGastoByCondominioOrderByDate(Condominio condominio,Pageable pageable);
	public Gasto findGastoById(Long id);
	
	//Pagos
	public void save(Pago pago);
	public Pago findPagoById(Long id);

	
	
	
}
