package com.tyfonware.app.models.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tyfonware.app.models.dao.ICondominioDao;
import com.tyfonware.app.models.entity.Condominio;

@Service
public class CondominioServiceImp implements ICondominioService {
	
	@Autowired
	private ICondominioDao condominioDao;
	
	@Override
	@Transactional(readOnly = true)
	public List<Condominio> findAll() {
		// TODO Auto-generated method stub
		return (List<Condominio>) condominioDao.findAll();
	}

	@Override
	@Transactional
	public void save(Condominio condominio) {
		condominioDao.save(condominio);
		
	}

	@Override
	@Transactional
	public void delete(Long id) {
		condominioDao.deleteById(id);		
	}

	@Transactional(readOnly = true)
	@Override
	public Condominio findCondominioByAdministrador(String username) {
		// TODO Auto-generated method stub
		return condominioDao.findCondominioByAdministrador(username);
	}
	
	@Transactional(readOnly = true)
	@Override
	public Condominio findById(Long id) {
		// TODO Auto-generated method stub
		return condominioDao.findById(id).orElse(null);
	}

}
