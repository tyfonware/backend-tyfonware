package com.tyfonware.app.models.dao;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.tyfonware.app.models.entity.Propietario;
import com.tyfonware.app.models.entity.Usuario;

public interface IPropietarioDao extends PagingAndSortingRepository<Propietario, Long>{
	
	@Query("select p from Propietario p join p.casas c join c.condominio con where con.id = ?1")
	public List<Propietario> findByIdCondominio(Long id);
	
	@Query("select p from Propietario p join p.casas c join c.condominio con where con.id = :idCondominio")
	public Page<Propietario> findAllByIdCondominio(@Param("idCondominio")Long id,Pageable pageable);
	
	@Query("select p from Propietario p join p.casas c join c.condominio con where con.id = :idCondominio and lower(concat(p.apellido,' ',p.nombre)) like lower(concat('%', :term,'%'))")
	public Page<Propietario> findAllByIdCondominio(@Param("idCondominio")Long id,@Param("term")String term,Pageable pageable);
	
	@Query("select p from Propietario p join p.usuario u where u.username = ?1 ")
	public Propietario findByUsernameUsuario(String username);
	
	public Propietario findByEmailOrTelefono(String email, String telefono);
	
	public Propietario findByUsuario(Usuario usuario);
	
}
