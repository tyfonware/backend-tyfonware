package com.tyfonware.app.models.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tyfonware.app.models.dao.IGastoDao;
import com.tyfonware.app.models.dao.IIngresoDao;
import com.tyfonware.app.models.dao.IPagoDao;
import com.tyfonware.app.models.entity.Condominio;
import com.tyfonware.app.models.entity.Gasto;
import com.tyfonware.app.models.entity.Ingreso;
import com.tyfonware.app.models.entity.Pago;

@Service
public class FinanzasServiceImp implements IFinanzasService{
	
	@Autowired
	private IIngresoDao ingresoDao;
	
	@Autowired
	private IGastoDao gastoDao;
	
	@Autowired
	private IPagoDao pagoDao;
	
	@Override
	@Transactional
	public void save(Ingreso ingreso) {
		ingresoDao.save(ingreso);
	}

	@Override
	@Transactional
	public void save(Gasto gasto) {
		gastoDao.save(gasto);
	}

	@Transactional(readOnly = true)
	@Override
	public List<Ingreso> findIngresoByCondominio(Condominio condominio) {
		// TODO Auto-generated method stub
		return ingresoDao.findByCondominio(condominio);
	}
	
	@Transactional(readOnly = true)
	@Override
	public List<Gasto> findGastoByCondominio(Condominio condominio) {
		// TODO Auto-generated method stub
		return gastoDao.findByCondominio(condominio);
	}
	
	@Transactional(readOnly = true)
	@Override
	public Page<Ingreso> findIngresoByCondominio(Condominio condominio, Pageable pageable) {
		// TODO Auto-generated method stub
		return ingresoDao.findByCondominio(condominio, pageable);
	}

	@Transactional(readOnly = true)
	@Override
	public Page<Gasto> findGastoByCondominio(Condominio condominio, Pageable pageable) {
		// TODO Auto-generated method stub
		return gastoDao.findByCondominio(condominio, pageable);
	}

	@Transactional(readOnly = true)
	@Override
	public List<Ingreso> findIngresoByCondominioOrderByDate(Condominio condominio, Pageable pageable) {
		// TODO Auto-generated method stub
		return ingresoDao.findByCondominioOrderByDate(condominio, pageable);
	}
	
	@Transactional(readOnly = true)
	@Override
	public List<Gasto> findGastoByCondominioOrderByDate(Condominio condominio, Pageable pageable) {
		// TODO Auto-generated method stub
		return gastoDao.findByCondominioOrderByDate(condominio, pageable);
	}
	
	@Transactional(readOnly = true)
	@Override
	public Ingreso findIngresoById(Long id) {
		// TODO Auto-generated method stub
		return ingresoDao.findById(id).orElse(null);
	}

	@Transactional(readOnly = true)
	@Override
	public Gasto findGastoById(Long id) {
		// TODO Auto-generated method stub
		return gastoDao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public void save(Pago pago) {
		// TODO Auto-generated method stub
		pagoDao.save(pago);
	}

	@Override
	@Transactional
	public Pago findPagoById(Long id) {
		// TODO Auto-generated method stub
		return pagoDao.findById(id).orElse(null);
	}

}
