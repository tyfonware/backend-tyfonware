package com.tyfonware.app.models.service;

import java.sql.Date;
import java.util.List;

import com.tyfonware.app.models.entity.Condominio;
import com.tyfonware.app.models.entity.DepositoBasura;
import com.tyfonware.app.models.entity.Recoleccion;
import com.tyfonware.app.models.entity.Residuo;
import com.tyfonware.app.models.entity.TipoResiduo;

public interface IResiduoService {

	
	
	//Recoleccion
	public void save(Recoleccion recoleccion);
	public Recoleccion findByDate(Date date);
	public Recoleccion findByCondominioAndDate(Condominio con, Date date);
	
	
	//Deposito Basura
	public void save(DepositoBasura deposito);
	public DepositoBasura findByCasaAndDate(Long casaID,Date date);
	
	//TipoResiduos
	public List<TipoResiduo> findAllTipoResiduo();
	public TipoResiduo findTipoResiduoById(Long id);
	
	//residuo
	public Residuo findResiduoById(Long id);

}
