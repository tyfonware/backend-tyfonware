package com.tyfonware.app.models.dao;

import org.springframework.data.repository.CrudRepository;

import com.tyfonware.app.models.entity.Evidencia;

public interface IEvidenciaDao extends CrudRepository<Evidencia, Long>{
	
}
