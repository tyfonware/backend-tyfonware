package com.tyfonware.app.models.dao;

import org.springframework.data.repository.CrudRepository;

import com.tyfonware.app.models.entity.TipoPago;

public interface ITipoPagoDao extends CrudRepository<TipoPago,Long>{
	
	
	
}
