package com.tyfonware.app.models.dao;

import java.sql.Date;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.tyfonware.app.models.entity.Condominio;
import com.tyfonware.app.models.entity.Recoleccion;


public interface IRecoleccionDao extends CrudRepository<Recoleccion, Long> {

	@Query("select r from Recoleccion r where r.createAt = ?1 ")
	public Recoleccion findByDate(Date date);
	
	@Query("select r from Recoleccion r join fetch r.condominio c where c = :con and r.createAt = :date order by r.id desc")
	public Recoleccion findByCondominioAndDate(@Param("con")Condominio con, @Param("date")Date date);
	
}
