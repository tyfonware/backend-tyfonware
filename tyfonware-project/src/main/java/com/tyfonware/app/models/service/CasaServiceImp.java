package com.tyfonware.app.models.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tyfonware.app.models.dao.ICasaDao;
import com.tyfonware.app.models.entity.Casa;
import com.tyfonware.app.models.entity.Condominio;

@Service
public class CasaServiceImp implements ICasaService {
	
	@Autowired
	private ICasaDao casaDao;
	
	@Override
	@Transactional(readOnly = true)
	public List<Casa> findAll() {
		// TODO Auto-generated method stub
		return (List<Casa>) casaDao.findAll();
	}

	@Override
	@Transactional
	public void save(Casa casa) {
		casaDao.save(casa);
		
	}

	@Override
	@Transactional(readOnly = true)
	public Casa findById(Long id) {
		// TODO Auto-generated method stub
		return casaDao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public void delete(Long id) {
		casaDao.deleteById(id);		
	}

	@Override
	public List<Casa> findByDireccion(String term) {
		// TODO Auto-generated method stub
		return casaDao.findByDireccionLikeIgnoreCase("%"+term+"%");
	}

	@Override
	public List<Casa> findByCondominio(Condominio condominio) {
		// TODO Auto-generated method stub
		return casaDao.findByCondominio(condominio);
	}

	@Override
	public Page<Casa> findByCondominio(Condominio condominio, Pageable pageable) {
		// TODO Auto-generated method stub
		return casaDao.findByCondominio(condominio, pageable);
	}

	@Override
	public List<Casa> findByCondominioAndDireccionLikeIgnoreCase(Condominio condominio, String term) {
		// TODO Auto-generated method stub
		return casaDao.findByCondominioAndDireccionLikeIgnoreCase(condominio, "%"+term+"%");
	}

	@Override
	public Page<Casa> findByCondominioAndDireccionLikeIgnoreCase(Condominio condominio, String term,
			Pageable pageable) {
		// TODO Auto-generated method stub
		return casaDao.findByCondominioAndDireccionLikeIgnoreCase(condominio, "%"+term+"%", pageable);
	}

}
