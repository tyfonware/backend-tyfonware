package com.tyfonware.app.models.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name="depositos_basura")
public class DepositoBasura implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private Integer estatus;
	
	//Getters and setters
	
	@OneToMany(fetch=FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name="id_depositio_basura")
	private List<RegistroResiduo> registroResiduos;
	
	@JsonManagedReference
	@ManyToOne(fetch=FetchType.LAZY)
	private Casa casa;
	
	@JsonBackReference
	@ManyToOne(fetch=FetchType.LAZY)
	private Recoleccion recoleccion;
	
	public Long getId() {
		return id;
	}

	
	public void setId(Long id) {
		this.id = id;
	}

	public Integer getEstatus() {
		return estatus;
	}


	public void setEstatus(Integer estatus) {
		this.estatus = estatus;
	}


	public List<RegistroResiduo> getRegistroResiduos() {
		return registroResiduos;
	}


	public void setRegistroResiduos(List<RegistroResiduo> registroResiduos) {
		this.registroResiduos = registroResiduos;
	}


	public Casa getCasa() {
		return casa;
	}


	public void setCasa(Casa casa) {
		this.casa = casa;
	}


	public Recoleccion getRecoleccion() {
		return recoleccion;
	}


	public void setRecoleccion(Recoleccion recoleccion) {
		this.recoleccion = recoleccion;
	}
	

	
	

}
