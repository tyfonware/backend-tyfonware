package com.tyfonware.app.util.validation;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.tyfonware.app.models.entity.Casa;

public class CasaValidator implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		// TODO Auto-generated method stub
		return Casa.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		
		Casa casa = (Casa)target;
		
		//Dirección
		
		String direccion = casa.getDireccion();
		Pattern pat = Pattern.compile("[a-zA-Z1-9À-ÖØ-öø-ÿ]+\\.?((\\s)[a-zA-Z1-9À-ÖØ-öø-ÿ]+\\.?)*");
	    Matcher mat = pat.matcher(direccion);
	    if(direccion == null || direccion == "") {
	    	errors.rejectValue("direccion", "casa.direccion", "Favor de llenar el campo");
	    } else {
	    	if (!mat.matches()) {
		        errors.rejectValue("direccion", "casa.direccion", "El nombre de la dirección no está permitido");
		    }if(direccion.length() > 30) { //Tamaño de la cadena
		        errors.rejectValue("direccion", "casa.direccion", "El nombre de la dirección debe estar entre los cinco y treinta carácteres");
		    }
	    }
	    
	    //Coordenadas
	    
	    Double latitud = casa.getLatitud();
	    Double longitud = casa.getLatitud();
	    if(longitud == null) {
	        errors.rejectValue("longitud", "casa.longitud", "Ocurrió un error, recarge la página e inténtelo de nuevo");
	    } else {
	    	if(latitud == null) {
		        errors.rejectValue("latitud", "casa.latitud", "Ocurrió un error, recarge la página e inténtelo de nuevo");
		    }if(latitud<-90 || latitud>90) {
		        errors.rejectValue("latitud", "casa.latitud", "Las coordenadas de la casa son incorrectas");
		    }if(longitud<-180 || longitud>180) {
		        errors.rejectValue("longitud", "casa.longitud", "Las coordenadas de la casa son incorrectas");
		    }
	    }
	    
	}

}
