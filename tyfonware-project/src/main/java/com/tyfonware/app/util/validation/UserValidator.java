package com.tyfonware.app.util.validation;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.tyfonware.app.models.entity.Usuario;

public class UserValidator implements Validator{

	@Override
	public boolean supports(Class<?> clazz) {
		// TODO Auto-generated method stub
		return Usuario.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		
		
		Usuario usuario = (Usuario) target;
		
		String username = usuario.getUsername();

		Pattern pat = Pattern.compile("[a-zA-Z0-9._-]{3,}");
		Matcher mat = pat.matcher(username);
		if(username == null || username == "") {
			errors.rejectValue("username", "usuario.username", "El nombre de usuario no puede ser vacío");
		} else {
			if (!mat.matches()) {
				errors.rejectValue("username", "usuario.username", "El nombre de usuario ingresado no es válido (Ingrese sólo letras, numeros, puntos y guiones)");
			}if(username.length() > 20 || username.length() <3) {
				errors.rejectValue("username", "usuario.username", "La longitud del nombre de usuario no es válido (Máximo 20 carácteres)");
			}
		}
		
		String password = usuario.getPassword();
		Pattern pat2 = Pattern.compile("^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,}$");
		mat = pat2.matcher(password);
		if(password == null || password == "") {
			errors.rejectValue("password", "usuario.password", "La contraseña no puede estar vacia");
		}else {
			if(password.length() > 32 || password.length() <8) {
				errors.rejectValue("password", "usuario.password", "La longitud de la contraseña del usuario no es válido (De 8 a 32 carácteres)");
			}if(!mat.matches()) {
				errors.rejectValue("password", "usuario.password", "La cotraseña debe tener una mayúscula, una minúscula, un carácter especial, un digito y sin espacios, por motivo de seguridad.");
			}
		}
		
	}

}
