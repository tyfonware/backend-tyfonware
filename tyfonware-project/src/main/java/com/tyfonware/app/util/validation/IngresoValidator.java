package com.tyfonware.app.util.validation;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.tyfonware.app.models.entity.Ingreso;

public class IngresoValidator implements Validator{

	@Override
	public boolean supports(Class<?> clazz) {
		// TODO Auto-generated method stub
		return Ingreso.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		
		Ingreso ingreso = (Ingreso)target;
		
		CharSequence cs1 = "<";
		CharSequence cs2 = ">";
		
		String nombre = ingreso.getNombre();
		Pattern pat = Pattern.compile("([A-ZÁÉÍÓÚ]{1}[a-zñáéíóú]+[\\s]*)+");
		Matcher mat = pat.matcher(nombre);
		if(nombre == null || nombre == "") {
			errors.rejectValue("nombre", "ingreso.nombre", "El campo no puede estar vacío");
		}else {
			if (!mat.matches()) {
				errors.rejectValue("nombre", "ingreso.nombre", "El nombre del campo no es válido");
			} if (nombre.length() > 20 && nombre.length() > 0) {
				errors.rejectValue("nombre", "ingreso.nombre", "Escriba un nombre válido");
			}
		}
		
		//
		
		String descripcion = ingreso.getDescripcion();
		if(descripcion == null || descripcion == "") {
			
		}else {
			if(descripcion.length() < 0 || descripcion.length() > 500) {
				errors.rejectValue("descripcion", "ingreso.descripcion", "Sólo se puede ingresar hasta 500 carácteres.");
			} if(descripcion.contains(cs1) || descripcion.contains(cs2)) {
				errors.rejectValue("descripcion", "ingreso.descripcion", "La descripción tiene carácteres invalidos");
			}
		}
		//
		Double monto = ingreso.getMonto();
		Pattern pat2 = Pattern.compile("^[0-9]+(\\.[0-9][0-9]?)?$");
		if(monto.isNaN() || monto == null) {
			errors.rejectValue("monto", "ingreso.monto", "El monto no puede estar vacío");
		}else {
			String montoString = Double.toString(monto);
			mat = pat2.matcher(montoString);
			if (!mat.matches()) {
				errors.rejectValue("monto", "ingreso.monto", "Sólo se permiten 2 decimales");
			}
			if (monto < 0 || monto > 999999) {
				errors.rejectValue("monto", "ingreso.monto", "El monto debe ser inferior a $1,000,000");
			}
			
		}
		
	}

}
