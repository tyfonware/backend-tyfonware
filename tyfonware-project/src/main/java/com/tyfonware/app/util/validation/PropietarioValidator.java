package com.tyfonware.app.util.validation;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.tyfonware.app.models.entity.Propietario;

public class PropietarioValidator implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {

		return Propietario.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {

		Propietario propietario = (Propietario) target;

		// Nombre
		String nombre = propietario.getNombre();

		Pattern pat = Pattern.compile("([A-ZÁÉÍÓÚ]{1}[a-zñáéíóú]+[\\s]*)+");
		Matcher mat = pat.matcher(nombre);
		if (nombre == null || nombre == "") {
				errors.rejectValue("nombre", "propietario.nombre", "Por favor llene el campo");
		} else {
			if (!mat.matches()) {
				errors.rejectValue("nombre", "propietario.nombre", "El nombre del propietario no es válido");
			} if (nombre.length() > 20 && nombre.length() > 0) {
				errors.rejectValue("nombre", "propietario.nombre", "Escriba un nombre válido");
			}
		}
		
		//Apellido
		
		String apellido = propietario.getApellido();
		mat = pat.matcher(apellido);
		if (nombre == null || nombre == "") {
			errors.rejectValue("apellido", "propietario.apellido", "Por favor llene el campo");
		} else {
			if (!mat.matches()) {
				errors.rejectValue("apellido", "propietario.apellido", "El apellido del propietario no es válido");
			} if  (nombre.length() > 20 && nombre.length() > 0) {
				errors.rejectValue("apellido", "propietario.apellido", "Escriba un apellido válido");
			} 
		}
		
		//Telefono \d
		String telefono = propietario.getTelefono();
		Pattern pat2 = Pattern.compile("\\d");
		mat = pat2.matcher(telefono);
		if(telefono == null || telefono == "") {
			errors.rejectValue("telefono", "propietario.telefono", "No deje en blanco el campo");
		} else {
			if(!mat.matches() || telefono.length()!=10) {
				errors.rejectValue("telefono", "propietario.telefono", "Escriba un teléfono celular de 10 dígitos");
			}
		}
		
		//Correo
		String email = propietario.getEmail();
		
		Pattern pat3 = Pattern.compile("(([^<>()\\[\\]\\\\.,;:\\s@\"]+(\\.[^<>()\\[\\]\\\\.,;:\\s@\"]+)*)|(\".+\"))@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))");
		mat = pat3.matcher(email);
		if(email == null || email == "") {
			errors.rejectValue("email","propietario.email", "Escriba un correo electrónico");
		} else {
			if(!mat.matches()) {
				errors.rejectValue("email","propietario.email", "El correo no es válido");
			} 
		}
	
	}

}
