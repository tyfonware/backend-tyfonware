package com.tyfonware.app.util.validation;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.tyfonware.app.models.entity.Condominio;

public class CondominioValidator implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		// TODO Auto-generated method stub
		return Condominio.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {

		Condominio condominio = (Condominio) target;

		String nombre = condominio.getNombre();
		Pattern pat = Pattern.compile("([A-ZÁÉÍÓÚ]{1}[a-zñáéíóú]+[\\\\s]*)+");
		Matcher mat = pat.matcher(nombre);
		if (!mat.matches()) {
			errors.rejectValue("nombre", "condominio.nombre", "El nombre del condominio no es válido");
		} else {
			if (nombre.length() > 30 && nombre.length() > 5) {
				errors.rejectValue("nombre", "condominio.nombre", "Escriba un condominio válido, de 5 a 30 carácteres");
			}
			if (nombre == null || nombre == "") {
				errors.rejectValue("nombre", "condominio.nombre", "Por favor llene el campo");
			}
		}

		// Coordenadas

		Double latitud = condominio.getLatitud();
		Double longitud = condominio.getLatitud();
		if (latitud == null || latitud.isNaN()) {
			errors.rejectValue("latitud", "condominio.latitud",
					"Ocurrió un error, recarge la página e inténtelo de nuevo");
		} else {
			if (latitud < -90 || latitud > 90) {
				errors.rejectValue("latitud", "condominio.latitud", "Las coordenadas de la casa son incorrectas");
			}
		}
		if (longitud == null || longitud.isNaN()) {
			errors.rejectValue("longitud", "condominio.longitud",
					"Ocurrió un error, recarge la página e inténtelo de nuevo");
		} else {
			if(longitud<-180 || longitud>180) {
				errors.rejectValue("longitud", "condominio.longitud", "Las coordenadas de la casa son incorrectas");
			}
		}

	}

}
