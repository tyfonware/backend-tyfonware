package com.tyfonware.app.util.validation;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.tyfonware.app.models.entity.Gasto;

public class GastoValidator implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		// TODO Auto-generated method stub
		return Gasto.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {

		Gasto gasto = (Gasto) target;

		String descripcion = gasto.getDescripcion();
		CharSequence cs1 = "<";
		CharSequence cs2 = ">";

		if (descripcion == null || descripcion == "") {
			errors.rejectValue("descripcion", "gasto.descripcion", "La descripción no puede estar vacia.");
		} else {
			if (descripcion.length() < 0 || descripcion.length() > 500) {
				errors.rejectValue("descripcion", "gasto.descripcion", "Sólo se puede ingresar hasta 500 carácteres.");
			}if (descripcion.contains(cs1) || descripcion.contains(cs2)) {
				errors.rejectValue("descripcion", "gasto.descripcion", "La descripción tiene carácteres invalidos");
			}
		}
		// ^[0-9]+(\.[0-9][0-9]?)?$
		Double monto = gasto.getMonto();
		
		Pattern pat = Pattern.compile("^[0-9]+(\\.[0-9][0-9]?)?$");

		if (monto == null || monto.isNaN()) {
			errors.rejectValue("monto", "gasto.monto", "El monto no puede estar vacio");
		} else {
			String montoString = Double.toString(monto);
			Matcher mat = pat.matcher(montoString);
			if (!mat.matches()) {
				errors.rejectValue("monto", "gasto.monto", "Sólo se permiten 2 decimales");
			}
			if (monto < 0 || monto > 999999) {
				errors.rejectValue("monto", "gasto.monto", "El monto debe ser inferior a $1,000,000");
			}
		}

	}

}