package com.tyfonware.app.controllers;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.tyfonware.app.models.entity.Casa;
import com.tyfonware.app.models.entity.Condominio;
import com.tyfonware.app.models.entity.Ingreso;
import com.tyfonware.app.models.entity.Pago;
import com.tyfonware.app.models.entity.Propietario;
import com.tyfonware.app.models.service.ICasaService;
import com.tyfonware.app.models.service.ICondominioService;
import com.tyfonware.app.models.service.IFinanzasService;
import com.tyfonware.app.models.service.IPropietarioService;
import com.tyfonware.app.util.paginator.PageRender;
import com.tyfonware.app.util.validation.IngresoValidator;

@SessionAttributes("ingreso")
@Controller
public class PagosController {
	
	@Autowired
	private IFinanzasService finanzasService;
	
	@Autowired
	private ICondominioService condominioService;
	
	@Autowired
	private IPropietarioService propietarioService;
	
	@Autowired
	private ICasaService casaService;
	
	@InitBinder("ingreso")
    protected void initBinderIngreso(WebDataBinder binder) {
		binder.setValidator(new IngresoValidator());
	}
	
	@Secured("ROLE_ADMIN")
	@RequestMapping(value="/admin/finanzas/nuevoIngreso", method = RequestMethod.POST)
	public String nuevoIngreso(Model model,RedirectAttributes flash,@Validated Ingreso ingreso,BindingResult result,  Authentication authentication) {
		if (result.hasErrors()) {
			model.addAttribute("titulo", "Nuevo ingreso");
			return "/admin/finanzas/nuevoIngreso";
		}
		Condominio condominio = condominioService.findCondominioByAdministrador(authentication.getName());
		ingreso.setCondominio(condominio);
		List<Pago> pagos = new ArrayList<>();
		
		for(Casa c: condominio.getCasas()) {
			Pago pago = new Pago();
			pago.setCasa(c);
			pago.setEstatus(0);
			pago.setMonto(0.0);
			pago.setIngreso(ingreso);
			//finanzasService.save(pago);
			pagos.add(pago);
		}
		
		String mensaje = "Se requiere *$"+ingreso.getMonto()+"* para "+ingreso.getNombre()+""
				+ ", con la finalidad de _'"+ingreso.getDescripcion()+"_'. Para más información, consulte al administrador de "
						+ "su condominio.";
		
		ingreso.setPagos(pagos);
		finanzasService.save(ingreso);
		flash.addFlashAttribute("mensaje", "Se ha creado el registro con éxito");
		
		
		//Notificación
		List<Propietario> propietarios =  propietarioService.findByIdCondominio(condominio.getId());
		Set<Propietario> hs = new HashSet<>();
		hs.addAll(propietarios);
		propietarios.clear();
		propietarios.addAll(hs);
		condominio.enviarNotificacion(mensaje, propietarios);
		//
		return "redirect:/admin/finanzas/";
	}
	
	@Secured("ROLE_ADMIN")
	@RequestMapping(value="/admin/finanzas/nuevoIngreso", method = RequestMethod.GET)
	public String formIngreso(Model model) {
		Ingreso ingreso = new Ingreso();
		model.addAttribute("Nuevo ingreso", ingreso);
		return "/admin/finanzas/nuevoIngreso";
	}
	
	@Secured("ROLE_ADMIN")
	@RequestMapping(value="/admin/finanzas/ingresos", method = RequestMethod.GET)
	public String verIngresos(@RequestParam(name="page",defaultValue = "0")int page,Model model,Authentication authentication) {
		
		Condominio con = condominioService.findCondominioByAdministrador(authentication.getName());
		Pageable pageRequest = PageRequest.of(page, 20);
		Page<Ingreso> ingresos = finanzasService.findIngresoByCondominio(con, pageRequest);
		PageRender<Ingreso> pageRender = new PageRender<>("/admin/propietarios/listar",ingresos);
		
		model.addAttribute("ingresos", ingresos);
		model.addAttribute("page", pageRender);
		model.addAttribute("titulo", "Ingresos");
		return "/admin/finanzas/ingresos";
	}
	
	@Secured("ROLE_ADMIN")
	@RequestMapping(value="/admin/finanzas/ingresos/{ingresoID}", method = RequestMethod.GET)
	public String verIngreso(RedirectAttributes flash,@PathVariable(value="ingresoID") Long ingresoID,Model model,Authentication authentication) {
		Condominio con = condominioService.findCondominioByAdministrador(authentication.getName());
		Ingreso ingreso = finanzasService.findIngresoById(ingresoID);
		if(ingreso.getCondominio() != con || ingreso == null) {
			flash.addFlashAttribute("mensaje", "No se ha encontrado el contenido que está buscando");
			return "redirect:/admin/finanzas/ingresos";
		}
		model.addAttribute("ingreso", ingreso);
		model.addAttribute("titulo", "Ingreso "+ingreso.getId());
		return "/admin/finanzas/ingreso";
		
	}
	
	@Secured("ROLE_ADMIN")
	@RequestMapping(value="/admin/finanzas/ingresos/pagar", method = RequestMethod.POST)
	public String pagar(SessionStatus status,RedirectAttributes flash,HttpServletRequest request,@RequestParam("montoPagar")Double monto,@RequestParam("pago_id")Long id,Authentication authentication,Ingreso ingreso) {
	    //Busca en la bd datos
		Pago pago = finanzasService.findPagoById(id);
		String referer = request.getHeader("Referer");
		Condominio con = condominioService.findCondominioByAdministrador(authentication.getName());
		if(pago.getIngreso().getId() != ingreso.getId()) {
			System.out.println(pago.getIngreso().getId() +" = "+ingreso.getId());
			flash.addFlashAttribute("mensaje", "No se pudo realizar la operación :(");
		    return "redirect:"+ referer;
		}
		if(ingreso.getCondominio().getId() != con.getId()) {
			flash.addFlashAttribute("mensaje", "No se pudo realizar la operación");
		    return "redirect:"+ referer;
		}
	    if(monto == null || monto == 0.0) {
			flash.addFlashAttribute("mensaje", "No se pudo realizar la operación, el pago no puede ser vacio");
		    return "redirect:"+ referer;
		}
	    //Variables
		Double requerido = ingreso.getMonto();
		Double montoTotal = monto + pago.getMonto();
		int estatus = 0;
		
		if(montoTotal > requerido) {
			flash.addFlashAttribute("mensaje", "El monto excede el adeudo");
		    return "redirect:"+ referer;
		} else if(monto <= 0) {
			flash.addFlashAttribute("mensaje", "No realice pagos de cantidades negativas");
		    return "redirect:"+ referer;
		}
		if(montoTotal.equals(requerido)) {
			estatus = 1;
			flash.addFlashAttribute("mensaje", "Se ha registrado el pago, el adeudo está completado");
			con.enviarNotificacion("Se ha completado el pago de *'"+ingreso.getNombre()+"'*, creado el "+ingreso.getCreateAt(), pago.getCasa().getPropietarios());
		} else {
			flash.addFlashAttribute("mensaje", "Se ha registrado el pago");
		}
		
		//Se guarda el ingreso primero para evitar problemas
		ingreso.setRecolectado(monto);
		finanzasService.save(ingreso);
		
		//Se añade dinero al condominio
		con.modificarSaldo(monto);
		condominioService.save(con);
		
		//Al final se guarda el pago
		pago.setMonto(montoTotal);
		pago.setEstatus(estatus);
		finanzasService.save(pago);
		
		status.setComplete();
		return "redirect:"+ referer;
	}
	
	/*
	 * 
	 *  P R O P I E T A R I O S
	 * 
	 */
	
	@Secured("ROLE_PROPIETARIO")
	@RequestMapping(value="/app/{casaID}/finanzas/ingresos", method = RequestMethod.GET)
	public String verIngresosPropietario(@PathVariable(value = "casaID") Long casaID,@RequestParam(name="page",defaultValue = "0")int page,Model model,Authentication authentication) {
		Boolean checarCasa = propietarioService.findByUsernameUsuario(authentication.getName()).validarCasa(casaID);
		if (!checarCasa) {
			return "redirect:/app/";
		}
		Casa casa = casaService.findById(casaID);
		Condominio con = casa.getCondominio();
		Pageable pageRequest = PageRequest.of(page, 20);
		Page<Ingreso> ingresos = finanzasService.findIngresoByCondominio(con, pageRequest);
		PageRender<Ingreso> pageRender = new PageRender<>("/admin/propietarios/listar",ingresos);
		
		model.addAttribute("ingresos", ingresos);
		model.addAttribute("page", pageRender);
		model.addAttribute("titulo", "Ingresos");
		model.addAttribute("casaID", casaID);
		return "/app/finanzas/ingresos";
		
	}
	
	@Secured("ROLE_PROPIETARIO")
	@RequestMapping(value="/app/{casaID}/finanzas/ingresos/{ingresoID}", method = RequestMethod.GET)
	public String verIngresoPropietario(@PathVariable(value = "casaID") Long casaID,RedirectAttributes flash,@PathVariable(value="ingresoID") Long ingresoID,Model model,Authentication authentication) {
		Boolean checarCasa = propietarioService.findByUsernameUsuario(authentication.getName()).validarCasa(casaID);
		if (!checarCasa) {
			return "redirect:/app/";
		}
		Casa casa = casaService.findById(casaID);
		Condominio con = casa.getCondominio();
		Ingreso ingreso = finanzasService.findIngresoById(ingresoID);
		if(ingreso.getCondominio() != con || ingreso == null) {
			flash.addFlashAttribute("mensaje", "No se ha encontrado el contenido que está buscando");
			return "redirect:/admin/finanzas/ingresos";
		}
		model.addAttribute("ingreso", ingreso);
		model.addAttribute("titulo", "Ingreso "+ingreso.getId());
		model.addAttribute("casaID", casaID);
		return "/app/finanzas/ingreso";
		
	}
}
