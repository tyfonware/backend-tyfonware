package com.tyfonware.app.controllers;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import com.tyfonware.app.models.entity.Condominio;
import com.tyfonware.app.models.entity.Gasto;
import com.tyfonware.app.models.entity.Ingreso;
import com.tyfonware.app.models.entity.Propietario;
import com.tyfonware.app.models.service.ICondominioService;
import com.tyfonware.app.models.service.IFinanzasService;
import com.tyfonware.app.models.service.IPropietarioService;
import com.tyfonware.app.util.validation.CondominioValidator;

@Controller
@SessionAttributes("condominio")
@RequestMapping(value = "/admin")
public class AdminController {

	@Autowired
	private ICondominioService condominioService;
	
	@Autowired
	private IPropietarioService propietarioService;
	
	@Autowired
	private IFinanzasService finanzasService;
	
	
	@InitBinder("condominio")
	protected void initBinder(WebDataBinder binder) {
		binder.setValidator(new CondominioValidator());
	}

	@Secured("ROLE_ADMIN")
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home(Model model) {
		model.addAttribute("titulo", "Inicio");
		return "/admin/home";

	}

	// C O N D O M I N I O

	@Secured("ROLE_ADMIN")
	@RequestMapping(value = "/condominio", method = RequestMethod.GET)
	public String condominio(Model model, Authentication authentication) {
		Condominio condominio = condominioService.findCondominioByAdministrador(authentication.getName());
		List<Propietario> p = propietarioService.findByIdCondominio(condominio.getId());
		Set<Propietario> hs = new HashSet<>();
		hs.addAll(p);
		p.clear();
		p.addAll(hs);
		model.addAttribute("propietarios", p);
		model.addAttribute("titulo", "Condominio");
		model.addAttribute("condominio", condominio);
		return "/admin/condominio";
	}

	@Secured("ROLE_ADMIN")
	@RequestMapping(value = "/condominio/editar", method = RequestMethod.GET)
	public String editarCondominio(Model model, Authentication authentication) {
		Condominio condominio = condominioService.findCondominioByAdministrador(authentication.getName());
		
		model.addAttribute("titulo", "Editar Condominio");
		model.addAttribute("condominio", condominio);
		return "/admin/editar-condominio";
	}

	@Secured("ROLE_ADMIN")
	@RequestMapping(value = "/condominio/editar", method = RequestMethod.POST)
	public String guardarCondominio(@Valid Condominio condominio, BindingResult result, Model model,
			SessionStatus status) {
		condominioService.save(condominio);

		if (result.hasErrors()) {
			model.addAttribute("titulo", "Editar condominio");
			return "/admin/editar-condominio";
		}

		status.setComplete();
		return "redirect:/admin/condominio";
	}

	@Secured("ROLE_ADMIN")
	@RequestMapping(value = "/notificacion", method = RequestMethod.POST)
	public String enviarNotificacion(Model model,Authentication authentication,@RequestParam("mensaje")String mensaje) {
		
		Condominio condominio = condominioService.findCondominioByAdministrador(authentication.getName());
		List<Propietario> propietarios =  propietarioService.findByIdCondominio(condominio.getId());
		Set<Propietario> hs = new HashSet<>();
		hs.addAll(propietarios);
		propietarios.clear();
		propietarios.addAll(hs);
		System.out.println(mensaje);
		//Mensaje
		condominio.enviarNotificacion(mensaje, propietarios);
		
		return "redirect:/admin/";
	}

	// FINANZAS
	@Secured("ROLE_ADMIN")
	@RequestMapping(value = "/finanzas/", method = RequestMethod.GET)
	public String finanzas(Model model,Authentication authentication) {
		Pageable topFive = PageRequest.of(0, 5);
		Condominio condominio = condominioService.findCondominioByAdministrador(authentication.getName());
		List<Ingreso> ingresos = finanzasService.findIngresoByCondominioOrderByDate(condominio, topFive);
		List<Gasto> gastos = finanzasService.findGastoByCondominioOrderByDate(condominio, topFive);
		
		
		model.addAttribute("titulo", "Finanzas");
		model.addAttribute("ingresos", ingresos);
		model.addAttribute("gastos", gastos);
		return "/admin/finanzas/inicio";
	}

}
