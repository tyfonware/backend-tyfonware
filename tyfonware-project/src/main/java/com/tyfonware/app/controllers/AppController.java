package com.tyfonware.app.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.tyfonware.app.models.entity.Casa;
import com.tyfonware.app.models.entity.Condominio;
import com.tyfonware.app.models.entity.Gasto;
import com.tyfonware.app.models.entity.Ingreso;
import com.tyfonware.app.models.entity.Propietario;
import com.tyfonware.app.models.service.ICasaService;
import com.tyfonware.app.models.service.IFinanzasService;
import com.tyfonware.app.models.service.IPropietarioService;
import com.tyfonware.app.models.service.IUsuarioService;

@Controller
@RequestMapping(value = "/app")
public class AppController {

	@Autowired
	private IPropietarioService propietarioService;

	@Autowired
	private IUsuarioService usuarioService;

	@Autowired
	private ICasaService casaService;
	
	@Autowired
	private IFinanzasService finanzasService;

	@Secured("ROLE_PROPIETARIO")
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String inicioPrevio(Model model, Authentication authentication) {

		Propietario propietario = propietarioService.findByUsernameUsuario(authentication.getName());

		model.addAttribute("titulo", "Inicio");
		model.addAttribute("propietario", propietario);
		return "/app/inicio-previo";
	}

	@Secured("ROLE_PROPIETARIO")
	@RequestMapping(value = "/{casaID}", method = RequestMethod.GET)
	public String home(@PathVariable(value = "casaID") Long casaID, Model model, Authentication authentication) {

		// Valida el si el id de la casa obtenia de la url corresponde a una que ya
		// tiene
		Boolean checarCasa = propietarioService.findByUsernameUsuario(authentication.getName()).validarCasa(casaID);

		if (!checarCasa) {

			return "redirect:/app/";
		}

		model.addAttribute("titulo", "Inicio");
		model.addAttribute("casaID", casaID);
		return "/app/home";
	}

	@Secured("ROLE_PROPIETARIO")
	@RequestMapping(value = "/{casaID}/casa/", method = RequestMethod.GET)
	public String verCasa(@PathVariable(value = "casaID") Long casaID, Model model, Authentication authentication) {

		Boolean checarCasa = propietarioService.findByUsernameUsuario(authentication.getName()).validarCasa(casaID);

		if (!checarCasa) {
			return "redirect:/app/";
		}

		Casa casa = casaService.findById(casaID);
		model.addAttribute("casa", casa);
		model.addAttribute("titulo", "Casa");
		model.addAttribute("casaID", casaID);
		return "/app/casa/casa";
	}
	
	@Secured("ROLE_PROPIETARIO")
	@RequestMapping(value = "/{casaID}/finanzas/", method = RequestMethod.GET)
	public String verFinanzas(@PathVariable(value = "casaID") Long casaID, Model model, Authentication authentication) {
		Boolean checarCasa = propietarioService.findByUsernameUsuario(authentication.getName()).validarCasa(casaID);
		if (!checarCasa) {
			return "redirect:/app/";
		}
		Casa casa = casaService.findById(casaID);
		Pageable topFive = PageRequest.of(0, 5);
		Condominio condominio = casa.getCondominio();
		List<Ingreso> ingresos = finanzasService.findIngresoByCondominioOrderByDate(condominio, topFive);
		List<Gasto> gastos = finanzasService.findGastoByCondominioOrderByDate(condominio, topFive);
		model.addAttribute("titulo", "Finanzas");
		model.addAttribute("ingresos", ingresos);
		model.addAttribute("gastos", gastos);
		model.addAttribute("casaID", casaID);
		return "/app/finanzas/inicio";
	}

}
