package com.tyfonware.app.controllers;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.tyfonware.app.models.entity.Casa;
import com.tyfonware.app.models.entity.Condominio;
import com.tyfonware.app.models.entity.Propietario;
import com.tyfonware.app.models.service.ICasaService;
import com.tyfonware.app.models.service.ICondominioService;
import com.tyfonware.app.models.service.IPropietarioService;
import com.tyfonware.app.util.paginator.PageRender;

@Controller
@RequestMapping(value="/admin/propietarios")
public class PropietarioController {
	
	@Autowired
	private IPropietarioService propietarioService;
	
	@Autowired
	private ICasaService casaService;
	
	@Autowired
	private ICondominioService condominioService;
	
	@Secured("ROLE_ADMIN")
	@RequestMapping(value="/listar", method = RequestMethod.GET)
	public String listar(@RequestParam(name="page",defaultValue = "0")int page,@RequestParam(name="nombre",defaultValue = "")String nombre,Model model,Authentication authentication) {
		Long idCondominio = condominioService.findCondominioByAdministrador(authentication.getName()).getId();
		
		Pageable pageRequest = PageRequest.of(page, 10);
		
		//Busca los propietarios en la BD
		Page<Propietario> propietarios = propietarioService.findAllByIdCondominio(idCondominio,nombre, pageRequest);
		
		List<Propietario> p = propietarioService.findByIdCondominio(idCondominio);
		
		Set<Propietario> hs = new HashSet<>();
		hs.addAll(p);
		p.clear();
		p.addAll(hs);
		
		PageRender<Propietario> pageRender = new PageRender<>("/admin/propietarios/listar",propietarios);
		//Atributos que manda al html
		Integer totalPropietarios =  p.size();
		p = null;
		model.addAttribute("totalPropietarios", totalPropietarios);
		model.addAttribute("titulo","Lista de Propietarios");
		model.addAttribute("propietarios", propietarios);
		model.addAttribute("page", pageRender);
		
		//Pagina a la que se dirige
		return "/admin/propietarios/listar";
	}
	
	@Secured("ROLE_ADMIN")
	@RequestMapping(value="/form", method = RequestMethod.GET)
	public String nuevoPropietario(Model model) {
		
		//Pagina a la que se dirige
		return "/admin/propietarios/form";
	}
	
	@Secured("ROLE_ADMIN")
	@RequestMapping(value="/form", method = RequestMethod.POST)
	public String guardar(@RequestParam("casas") Long[] casas,@RequestParam("username") String username, Model model, RedirectAttributes flash) {
		

		
		Propietario propietario = propietarioService.findByUsernameUsuario(username);
		if(propietario == null) {
			flash.addFlashAttribute("mensaje", "No se agregó al propietario");
			return "redirect:/admin/propietarios/listar";
		} 
		List<Casa> listaCasas = new ArrayList<Casa>();
		for (int i = 0; i < casas.length; i++) {
			listaCasas.add(casaService.findById(casas[i]));
		}
		
		List<Casa> casasList = propietario.getCasas();
		casasList.addAll(listaCasas);
		Set<Casa> hs = new HashSet<>();
		hs.addAll(casasList);
		casasList.clear();
		casasList.addAll(hs);
		
		propietario.setCasas(casasList);		
		propietarioService.save(propietario);
		//Pagina a la que se dirige
		flash.addFlashAttribute("mensaje", "Se agregó al propietario");
		return "redirect:/admin/propietarios/listar";
	}
	
	
	@Secured("ROLE_ADMIN")
	@RequestMapping(value="/propietario/{id}", method = RequestMethod.GET)
	public String verPropietario(@PathVariable(value="id") Long id,Model model,Authentication authentication) {
		Propietario propietario = propietarioService.findById(id);
		List<Casa> casas = propietario.getCasas();
		Boolean permitir = false;
		List<Casa> casasCondominio = new ArrayList<>();
		Condominio condominio = condominioService.findCondominioByAdministrador(authentication.getName());

		for(Casa c: casas) {
			if(propietario.validarCasa(c.getId())) {
				permitir = true;
			}
			if(condominio.getId() == c.getCondominio().getId()) {
				casasCondominio.add(c);
			}
		}
		if(!permitir) {
			return "redirect:/admin/propietarios/listar";
		}
		//Pagina a la que se dirige
		model.addAttribute("titulo", "Ver propietario");
		model.addAttribute("propietario", propietario);
		model.addAttribute("casas", casasCondominio);
		return "/admin/propietarios/propietario";
	}
	
	//Editar relación con condominio
	@Secured("ROLE_ADMIN")
	@GetMapping(value = "/mostrar-casas/{term}", produces = { "application/json" })
	public @ResponseBody List<Casa> mostrarCasas(@PathVariable String term,Authentication authentication){
		Condominio con = condominioService.findCondominioByAdministrador(authentication.getName());
		return casaService.findByCondominioAndDireccionLikeIgnoreCase(con, term);
	}
	
	
	
}
