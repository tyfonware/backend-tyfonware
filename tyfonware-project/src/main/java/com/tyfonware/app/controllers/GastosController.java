package com.tyfonware.app.controllers;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.tyfonware.app.models.entity.Casa;
import com.tyfonware.app.models.entity.Condominio;
import com.tyfonware.app.models.entity.Evidencia;
import com.tyfonware.app.models.entity.Gasto;
import com.tyfonware.app.models.service.ICasaService;
import com.tyfonware.app.models.service.ICondominioService;
import com.tyfonware.app.models.service.IFinanzasService;
import com.tyfonware.app.models.service.IPropietarioService;
import com.tyfonware.app.models.service.IUploadFileService;
import com.tyfonware.app.util.paginator.PageRender;
import com.tyfonware.app.util.validation.GastoValidator;

@Controller
public class GastosController {

	@Autowired
	private IUploadFileService uploadFileService;

	@Autowired
	private IFinanzasService finanzasService;
	
	@Autowired
	private ICondominioService condominioService;
	
	@Autowired
	private IPropietarioService propietarioService;
	
	@Autowired
	private ICasaService casaService;
	
	@InitBinder("gasto")
    protected void initBinderGasto(WebDataBinder binder) {
		binder.setValidator(new GastoValidator());
	}

	@GetMapping(value = "/uploads/{filename:.+}")
	public ResponseEntity<Resource> verFoto(@PathVariable String filename) {

		Resource recurso = null;

		try {
			recurso = uploadFileService.load(filename);
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return ResponseEntity.ok()
				.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + recurso.getFilename() + "\"")
				.body(recurso);
	}
	
	//
	//
	//
	
	@Secured("ROLE_ADMIN")
	@RequestMapping(value = "/admin/finanzas/nuevoGasto", method = RequestMethod.GET)
	public String gastoForm(Model model) {

		Gasto gasto = new Gasto();
		model.addAttribute("titulo", "Nuevo gasto");
		model.addAttribute("gasto", gasto);
		return "/admin/finanzas/nuevoGasto";

	}

	@Secured("ROLE_ADMIN")
	@RequestMapping(value = "/admin/finanzas/nuevoGasto", method = RequestMethod.POST)
	public String nuevoGasto(RedirectAttributes flash,HttpServletRequest request,@RequestParam("fotos") List<MultipartFile> fotos, @Validated Gasto gasto,BindingResult result,Authentication authentication,Model model) {
		
		if (result.hasErrors()) {
			model.addAttribute("titulo", "Nuevo gasto");
			return "/admin/finanzas/nuevoGasto";
		}
		
		String referer = request.getHeader("Referer");
		Condominio con = condominioService.findCondominioByAdministrador(authentication.getName());
		gasto.setCondominio(con);
		List<Evidencia> evidencias = new ArrayList<>();
		if(!con.modificarSaldo(-gasto.getMonto())) {
			flash.addFlashAttribute("mensaje", "No se pudo realizar la operación, el condominio no tiene suficiente saldo [Saldo: $"+con.getSaldo()+"]");
		    return "redirect:"+ referer;
		}
		for (MultipartFile foto : fotos) {
			System.out.println(foto);
			System.out.println(foto.getContentType());
			if (!foto.equals(null)) {
				if ((foto.getContentType().toLowerCase().equals("image/jpg")
						|| foto.getContentType().toLowerCase().equals("image/jpeg")
						|| foto.getContentType().toLowerCase().equals("image/png"))) {
					String uniqueFilename = null;
					try {
						uniqueFilename = uploadFileService.copy(foto);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					Evidencia evidencia = new Evidencia();
					evidencia.setImagen(uniqueFilename);
					evidencias.add(evidencia);
				}
			}
		}
		gasto.setEvidencias(evidencias);
		finanzasService.save(gasto);
		condominioService.save(con);
		flash.addFlashAttribute("mensaje", "Se ha creado el registro con éxito");
		return "redirect:/admin/finanzas/";
	}
	
	//Ver los gastos
	@Secured("ROLE_ADMIN")
	@RequestMapping(value="/admin/finanzas/gastos", method = RequestMethod.GET)
	public String verGastos(@RequestParam(name="page",defaultValue = "0")int page,Model model,Authentication authentication) {
		Condominio con = condominioService.findCondominioByAdministrador(authentication.getName());
		Pageable pageRequest = PageRequest.of(page, 20);
		Page<Gasto> gastos = finanzasService.findGastoByCondominio(con, pageRequest);
		PageRender<Gasto> pageRender = new PageRender<>("/admin/propietarios/listar",gastos);
		model.addAttribute("gastos", gastos);
		model.addAttribute("page", pageRender);
		model.addAttribute("titulo", "Gastos");
		return "/admin/finanzas/gastos";
	}
	
	@Secured("ROLE_ADMIN")
	@RequestMapping(value="/admin/finanzas/gastos/{gastoID}", method = RequestMethod.GET)
	public String verGasto(RedirectAttributes flash,@PathVariable(value="gastoID") Long gastoID,Model model,Authentication authentication) {
		Condominio con = condominioService.findCondominioByAdministrador(authentication.getName());
		Gasto gasto = finanzasService.findGastoById(gastoID);
		if(gasto == null || gasto.getCondominio() != con) {
			flash.addFlashAttribute("mensaje", "No se ha encontrado el contenido que está buscando");
			return "redirect:/admin/finanzas/gastos";
		} 
		model.addAttribute("gasto", gasto);
		model.addAttribute("titulo", "Gasto "+gasto.getId());
		return "/admin/finanzas/gasto";
	}
	
	/*
	 * 
	 *  P R O P I E T A R I O
	 * 
	 */
	
	@Secured("ROLE_PROPIETARIO")
	@RequestMapping(value = "/app/{casaID}/finanzas/gastos", method = RequestMethod.GET)
	public String verGastosPropietario(@PathVariable(value = "casaID") Long casaID,@RequestParam(name="page",defaultValue = "0")int page,Model model,Authentication authentication) {
		Boolean checarCasa = propietarioService.findByUsernameUsuario(authentication.getName()).validarCasa(casaID);
		if (!checarCasa) {
			return "redirect:/app/";
		}
		Casa casa = casaService.findById(casaID);
		Condominio con = casa.getCondominio();
		Pageable pageRequest = PageRequest.of(page, 20);
		Page<Gasto> gastos = finanzasService.findGastoByCondominio(con, pageRequest);
		PageRender<Gasto> pageRender = new PageRender<>("/admin/propietarios/listar",gastos);
		model.addAttribute("gastos", gastos);
		model.addAttribute("page", pageRender);
		model.addAttribute("titulo", "Gastos");
		model.addAttribute("casaID", casaID);
		return "/app/finanzas/gastos";
	}
	
	@Secured("ROLE_PROPIETARIO")
	@RequestMapping(value="/app/{casaID}/finanzas/gastos/{gastoID}", method = RequestMethod.GET)
	public String verGastoPropietario(HttpServletRequest request,@PathVariable(value = "casaID") Long casaID,RedirectAttributes flash,@PathVariable(value="gastoID") Long gastoID,Model model,Authentication authentication) {
		Boolean checarCasa = propietarioService.findByUsernameUsuario(authentication.getName()).validarCasa(casaID);
		if (!checarCasa) {
			return "redirect:/app/";
		}
		Casa casa = casaService.findById(casaID);
		Condominio con = casa.getCondominio();
		Gasto gasto = finanzasService.findGastoById(gastoID);
		String referer = request.getHeader("Referer");
		if(gasto == null || gasto.getCondominio() != con) {
			flash.addFlashAttribute("mensaje", "No se ha encontrado el contenido que está buscando");
		    return "redirect:"+ referer;
		} 
		model.addAttribute("gasto", gasto);
		model.addAttribute("titulo", "Gasto "+gasto.getId());
		model.addAttribute("casaID", casaID);
		return "/app/finanzas/gasto";
	}
	
}
