package com.tyfonware.app.controllers;


import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.tyfonware.app.models.entity.Casa;
import com.tyfonware.app.models.entity.Condominio;
import com.tyfonware.app.models.entity.Propietario;
import com.tyfonware.app.models.service.ICasaService;
import com.tyfonware.app.models.service.ICondominioService;
import com.tyfonware.app.models.service.IPropietarioService;
import com.tyfonware.app.util.paginator.PageRender;
import com.tyfonware.app.util.validation.CasaValidator;

@Controller
@RequestMapping(value="/admin/casas")
@SessionAttributes("casa")
public class CasaController {
	
	@Autowired
	private ICasaService casaService;
	
	@Autowired
	private ICondominioService condominioService;
	
	@Autowired
	private IPropietarioService propietarioService;
	
	@InitBinder("casa")
    protected void initBinder(WebDataBinder binder) {
        binder.setValidator(new CasaValidator());
    }
	
	
	@Secured("ROLE_ADMIN")
	@RequestMapping(value="/listar", method = RequestMethod.GET)
	public String listar(@RequestParam(name="page",defaultValue = "0")int page, @RequestParam(name="direccion",defaultValue = "")String direccion,Authentication authentication,Model model) {	
		Pageable pageRequest = PageRequest.of(page, 20);
		Condominio condominio = condominioService.findCondominioByAdministrador(authentication.getName());
		Page<Casa> casas = casaService.findByCondominioAndDireccionLikeIgnoreCase(condominio, direccion,pageRequest);
		PageRender<Casa> pageRender = new PageRender<>("/admin/casas/listar",casas);
		Integer totalCasas = casaService.findByCondominio(condominio).size();
		model.addAttribute("totalCasas", totalCasas);
		model.addAttribute("titulo","Casas");
		model.addAttribute("casas", casas);
		model.addAttribute("page", pageRender);
		return "/admin/casas/listar";
		
	}
	
	//Para mostrar
	@Secured("ROLE_ADMIN")
	@RequestMapping(value="/form", method = RequestMethod.GET)
	public String nuevaCasa(Model model) {
		Casa casa = new Casa();
		model.addAttribute("casa",casa);
		model.addAttribute("titulo","Nuevo registro de casa");
		return "/admin/casas/form";
	}
	//Para guardar
	@Secured("ROLE_ADMIN")
	@RequestMapping(value="/form", method = RequestMethod.POST)
	public String guardarCasa(RedirectAttributes flash,@Validated Casa casa,BindingResult result,Model model,SessionStatus status,Authentication authentication) {
		
		if (result.hasErrors()) {
			model.addAttribute("titulo", "Registro de casa");
			return "/admin/casas/form";
		}
		
		Condominio condominio = condominioService.findCondominioByAdministrador(authentication.getName());
		flash.addFlashAttribute("mensaje", "Casa guardada");
		casa.setCondominio(condominio);
		casaService.save(casa);
		status.setComplete();
		
		return "redirect:/admin/casas/listar";
	}
	
	@Secured("ROLE_ADMIN")
	@RequestMapping(value="/eliminar/{id}")
	public String eliminar(@PathVariable(value="id") Long id, RedirectAttributes flash, Authentication authentication) {
		if(id > 0) {
			Casa casa = casaService.findById(id);
			Condominio condominio = condominioService.findCondominioByAdministrador(authentication.getName());
			if(casa.getCondominio().getId() == condominio.getId()) {
				casaService.delete(id);
				flash.addFlashAttribute("mensaje", "Casa eliminada con éxito");
			} else {
				flash.addFlashAttribute("mensaje", "No se puede eliminar la casa");
			}
		} else {
			flash.addFlashAttribute("mensaje", "No se puede eliminar la casa");
		}
		return "redirect:/admin/casas/listar";
	}
	
	//Editar
	@Secured("ROLE_ADMIN")
	@RequestMapping(value="/form/{id}")
	public String editar(@PathVariable(value="id") Long id, Model model) {
		
		Casa casa = null;
		
		if(id > 0) {
			casa = casaService.findById(id);
			if(casa == null) {
				return "redirect:/admin/casas/listar";
			}
		} else {
			return "redirect:/admin/casas/listar";
		}
		model.addAttribute("titulo","Editar casa");
		model.addAttribute("casa",casa);
		return "/admin/casas/form";
	}
	
	@Secured("ROLE_ADMIN")
	@RequestMapping(value="/mapa")
	public String mapa(Model model) {
		model.addAttribute("titulo", "Mapa de casas");
		return "/admin/casas/mapa";
	}
	
	//Ver casa en específico
	@Secured("ROLE_ADMIN")
	@RequestMapping(value="/casa/{id}")
	public String verCasa(@PathVariable(value="id") Long id, Model model) {
		Casa casa = casaService.findById(id);
		model.addAttribute("casa", casa);
		model.addAttribute("titulo", "Ver casa");
		return "/admin/casas/casa";
	}
	
	@Secured("ROLE_ADMIN")
	@RequestMapping(value="/casa/{id}/eliminarPropietario/{propietarioID}")
	public String eliminarPropietario(@PathVariable(value="id") Long id,@PathVariable(value="propietarioID") Long propietarioID, Model model, Authentication authentication) {
		Condominio condominio = condominioService.findCondominioByAdministrador(authentication.getName());
		Casa casa = casaService.findById(id);
		Propietario propietario = propietarioService.findById(propietarioID);
		Boolean permitir = false;
		for(Casa c : condominio.getCasas()) {
			if(c.getId() == id) {
				permitir = true;
			}
		}
		if(permitir) {
			
			permitir = false;
			for(Casa c : propietario.getCasas()) {
				if(c.getId() == id) {
					permitir = true;
				}
			}
			if(permitir) {
				
				propietario.getCasas().remove(casa);
				propietarioService.save(propietario);
			}
			
		}
		return "redirect:/admin/casas/casa/"+id;
	}
	
	//APIS (Para obtener JSON)
	
	@GetMapping(value = "/condominioCoordenadas", produces = { "application/json" })
	public @ResponseBody Condominio obtenerCoordenadas(Authentication authentication){
		Condominio condominio = condominioService.findCondominioByAdministrador(authentication.getName());
		System.out.println(condominio.getLatitud());
		return condominio;
	}
	
	@GetMapping(value = "/casasRest", produces = { "application/json" })
	public @ResponseBody List<Casa> obtenerCasas(Authentication authentication){
		Condominio condominio = condominioService.findCondominioByAdministrador(authentication.getName());
		List<Casa> casas = casaService.findByCondominio(condominio);
		return casas;
	}
	
}
