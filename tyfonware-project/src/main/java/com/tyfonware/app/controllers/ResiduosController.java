	package com.tyfonware.app.controllers;

import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.tyfonware.app.models.entity.Casa;
import com.tyfonware.app.models.entity.Condominio;
import com.tyfonware.app.models.entity.DepositoBasura;
import com.tyfonware.app.models.entity.Recoleccion;
import com.tyfonware.app.models.entity.RegistroResiduo;
import com.tyfonware.app.models.entity.Residuo;
import com.tyfonware.app.models.entity.TipoResiduo;
import com.tyfonware.app.models.service.ICasaService;
import com.tyfonware.app.models.service.ICondominioService;
import com.tyfonware.app.models.service.IPropietarioService;
import com.tyfonware.app.models.service.IResiduoService;

@Controller
public class ResiduosController {

	@Autowired
	private IResiduoService residuoService;

	@Autowired
	private ICondominioService condominioService;

	@Autowired
	private IPropietarioService propietarioService;

	@Autowired
	private ICasaService casaService;

	// A D M I N

	@Secured("ROLE_ADMIN")
	@RequestMapping(value = "/admin/residuos/", method = RequestMethod.GET)
	public String inicioAdmin(Model model,Authentication authentication) {
		Date date = Date.valueOf(LocalDate.now());
		Condominio condominio = condominioService.findCondominioByAdministrador(authentication.getName());
		Recoleccion recoleccion = residuoService.findByCondominioAndDate(condominio, date);
		model.addAttribute("recoleccion",recoleccion);
		model.addAttribute("titulo", "Residuos");
		return "/admin/residuos/inicio";
	}

	@Secured("ROLE_ADMIN")
	@RequestMapping(value = "/admin/residuos/iniciar", method = RequestMethod.GET)
	public String iniciarDeposito(Model model, Authentication authentication) {
		Condominio condominio = condominioService.findCondominioByAdministrador(authentication.getName());
		Date date = Date.valueOf(LocalDate.now());
		Recoleccion r = residuoService.findByCondominioAndDate(condominio, date);
		Recoleccion recoleccion = new Recoleccion();
		if(r == null || r.getEstatus() == 1) {
			recoleccion.setCondominio(condominio);
			recoleccion.setEstatus(0);
			residuoService.save(recoleccion);	
		}
		r = null;	
		model.addAttribute("titulo", "Residuos");
		return "/admin/residuos/ruta";
	}
	
	
	@Secured("ROLE_ADMIN")
	@RequestMapping(value = "/admin/residuos/finalizar", method = RequestMethod.GET)
	public String finalizarDeposito(Model model, Authentication authentication) {
		Date date = Date.valueOf(LocalDate.now());
		Condominio condominio = condominioService.findCondominioByAdministrador(authentication.getName());
		Recoleccion recoleccion = residuoService.findByCondominioAndDate(condominio, date);
		if(recoleccion != null || recoleccion.getEstatus() != 1) {
			recoleccion.setEstatus(1);
			residuoService.save(recoleccion);	
		}
		return "/admin/residuos/ruta";
	}
	/*
	 * 
	 * P R O P I E T A R I O S Mandar casaID en todos los controladores
	 */

	@Secured("ROLE_PROPIETARIO")
	@RequestMapping(value = "/app/{casaID}/residuos/", method = RequestMethod.GET)
	public String inicioPropietario(@PathVariable(value = "casaID") Long casaID, Model model,
			Authentication authentication) {

		Boolean checarCasa = propietarioService.findByUsernameUsuario(authentication.getName()).validarCasa(casaID);
		if (!checarCasa) {

			return "redirect:/app/";
		}

		model.addAttribute("titulo", "Residuos");
		model.addAttribute("casaID", casaID);
		return "/app/residuos/inicio";
	}

	@Secured("ROLE_PROPIETARIO")
	@RequestMapping(value = "/app/{casaID}/residuos/registro", method = RequestMethod.GET)
	public String depositoBasura(@PathVariable(value = "casaID") Long casaID, Model model,
			Authentication authentication) {

		Boolean checarCasa = propietarioService.findByUsernameUsuario(authentication.getName()).validarCasa(casaID);
		if (!checarCasa) {
			return "redirect:/app/";
		}

		Condominio condominio = casaService.findById(casaID).getCondominio();
		Date date = Date.valueOf(LocalDate.now());
		Recoleccion recoleccion = residuoService.findByCondominioAndDate(condominio, date);
		
		if(recoleccion == null || recoleccion.getEstatus() == 1) {
			return "redirect:/app/"+casaID+"/residuos/";
		}
		
		DepositoBasura d = residuoService.findByCasaAndDate(casaID, date);
		DepositoBasura deposito = new DepositoBasura();
		if(d == null) {
			deposito.setCasa(casaService.findById(casaID));
			deposito.setEstatus(0);
			deposito.setRecoleccion(recoleccion);
			residuoService.save(deposito);
		} else if(d.getEstatus() == 1) {
			return "redirect:/app/"+casaID+"/residuos/";
		}
		
		d=null;
		condominio=null;
		
		
		List<TipoResiduo> tResiduos=residuoService.findAllTipoResiduo();
		model.addAttribute("tResiduos", tResiduos);
		model.addAttribute("casaID", casaID);
		return "/app/residuos/registro-basura";
		
	}
	
	@Secured("ROLE_PROPIETARIO")
	@RequestMapping(value = "/app/{casaID}/residuos/registro", method = RequestMethod.POST)
	public String depositarBasura(@RequestParam("residuo")Long[] residuosID, @RequestParam("cantidad")Integer[] cantidades, @PathVariable(value = "casaID") Long casaID, Model model,
			Authentication authentication) {
		
		
		//Aqui checa que la casa es de el
		Boolean checarCasa = propietarioService.findByUsernameUsuario(authentication.getName()).validarCasa(casaID);
		if (!checarCasa) {
			return "redirect:/app/";
		}
		Date date = Date.valueOf(LocalDate.now());
		
		//Aqui checa el estatus del deposito
		DepositoBasura deposito = residuoService.findByCasaAndDate(casaID, date);
		if(deposito == null || deposito.getEstatus() == 01) {
			return "redirect:/app/"+casaID+"/residuos/";
		}
		
		List<Residuo> residuos = new ArrayList<>();
		
		//Busca los residuos en la bd por el id
		for(Long i: residuosID) {
			residuos.add(residuoService.findResiduoById(i));
		}
		
		Set<RegistroResiduo> registros = new LinkedHashSet<>();
		int cont = 0;
		
		//Agregra los registros sin repeticiones
		for(Residuo residuo: residuos) {
			RegistroResiduo registro = new RegistroResiduo();
			registro.setCantidad(cantidades[cont]);
			registro.setResiduo(residuo);
			registros.add(registro);
			cont++;
		}
		residuos = null;
		Casa casa =  casaService.findById(casaID);
		Condominio condominio = casa.getCondominio();
		Recoleccion recoleccion = residuoService.findByCondominioAndDate(condominio, date);
		List<RegistroResiduo> registrosFinal = new ArrayList<>(registros);
		registros = null;
		//Aqui se empieza añadir todo y guardar en la bd
		deposito.setRegistroResiduos(registrosFinal);
		deposito.setCasa(casa);
		deposito.setRecoleccion(recoleccion);
		deposito.setEstatus(1);
		residuoService.save(deposito);
		
		
		return "redirect:/app/"+casaID+"/residuos/";
	}
	
	//rest
	@GetMapping(value = "/app/rest/residuos/{id}", produces = { "application/json" })
	public @ResponseBody TipoResiduo obtenerResiduos(@PathVariable Long id){
		return residuoService.findTipoResiduoById(id);
	}
	
	@Secured("ROLE_ADMIN")
	@GetMapping(value = "/admin/rest/residuos/recoleccion", produces = { "application/json" })
	public @ResponseBody Recoleccion obtenerRecoleccion(Authentication authentication){
		Date date = Date.valueOf(LocalDate.now());
		Condominio condominio = condominioService.findCondominioByAdministrador(authentication.getName());
		return residuoService.findByCondominioAndDate(condominio, date);
	}

}
