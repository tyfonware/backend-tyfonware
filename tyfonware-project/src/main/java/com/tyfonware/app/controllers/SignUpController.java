package com.tyfonware.app.controllers;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.tyfonware.app.models.entity.Propietario;
import com.tyfonware.app.models.entity.Role;
import com.tyfonware.app.models.entity.Usuario;
import com.tyfonware.app.models.service.IPropietarioService;
import com.tyfonware.app.models.service.IUsuarioService;
import com.tyfonware.app.util.validation.PropietarioValidator;
import com.tyfonware.app.util.validation.UserValidator;

@Controller
public class SignUpController {
	
	@Autowired
	private BCryptPasswordEncoder passwordEncoder;
	
	@Autowired
	private IPropietarioService propietarioService;
	
	@Autowired
	private IUsuarioService usuarioService;
	
	@InitBinder("propietario")
    protected void initBinderPropietario(WebDataBinder binder) {
        binder.setValidator(new PropietarioValidator());
    }
	
	@InitBinder("usuario")
    protected void initBinderUsuario(WebDataBinder binder) {
        binder.setValidator(new UserValidator());
    }
	
	@RequestMapping(value = "/signup", method = RequestMethod.GET)
	public String registro(Model model) {

		Propietario propietario = new Propietario();
		Usuario usuario = new Usuario();
		model.addAttribute("propietario", propietario);
		model.addAttribute("usuario", usuario);
		model.addAttribute("titulo", "Registrar usuario");

		return "/signup";
	}

	@RequestMapping(value = "/signup", method = RequestMethod.POST)
	public String registrar(RedirectAttributes flash,@Valid Propietario propietario,BindingResult resultPropietario,@Valid Usuario usuario,BindingResult resultUsuario, Model model) {
		
		if (resultPropietario.hasErrors() || resultUsuario.hasErrors()) {
			model.addAttribute("titulo", "Registrarse");
			return "/signup";
		}
		
		Propietario validacionPropietario = propietarioService.findByEmailOrTelefono(propietario.getEmail(), propietario.getTelefono());
		if(validacionPropietario == null) {
			System.out.println("Propietario no encontrado, siguiente...");
		}
		else if(validacionPropietario.getId() > 0) {
			flash.addFlashAttribute("mensaje", "Ya existe un propietario con el mismo correo o número de teléfono");
			return "redirect:/signup";
		}
		
		Usuario validacionUsuario = usuarioService.findByUsername(usuario.getUsername());
		if(validacionUsuario == null) {
			System.out.println("Usuario no encontrado, siguiente...");
		}
		else if(validacionUsuario.getId() > 0) {
			flash.addFlashAttribute("mensaje", "Ya existe un usuario con ese nombre de usuario");
			return "redirect:/signup";
		}
		
		List<Role> roles = new ArrayList<Role>();
		Role role = new Role();
		
		role.setAuthority("ROLE_PROPIETARIO");
		roles.add(role);
		
		String passwordCifrada = null;
		for(int i=0; i<1; i++) {
			String bcryptPassword = passwordEncoder.encode(usuario.getPassword());
			passwordCifrada = bcryptPassword;
		}
		
		usuario.setPassword(passwordCifrada);
		usuario.setRoles(roles);
		usuario.setEnabled(true);
		
		propietario.setUsuario(usuario);
		propietarioService.save(propietario);
		
		flash.addFlashAttribute("mensaje", "Usuario creado con exito");
		return "redirect:/";
	}

}
