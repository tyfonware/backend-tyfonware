const categoria = document.querySelector('#tipoResiduo');
const checkContainer = document.querySelector('#checkContainer');
const formResiduos = document.querySelector("#formResiduos");
const btnAgregar = document.querySelector('#agregar');
const btnBuscar = document.querySelector('#buscar');
const registroResiduos = document.querySelector("#registroResiduos");

// Eventos
btnAgregar.addEventListener('click',agregarDeposito);
formResiduos.addEventListener('submit',e=> e.preventDefault());
btnBuscar.addEventListener('click',buscar);
registroResiduos.addEventListener('click',borrarDeposito);
// funciones

const request = async () => {
	let id = categoria.value;
	const response = await fetch('/app/rest/residuos/'+id);
	const json = await response.json();
	return json;
}

async function buscar(e){
	let id = categoria.value;
	const response = await fetch('/app/rest/residuos/'+id);
	const json = await response.json();
	while (checkContainer.firstChild) {
		checkContainer.removeChild(checkContainer.firstChild);
	}
	let residuos = json.residuos;
	
	checkContainer.innerHTML =`<div class="progress">
      <div class="indeterminate"></div>
  </div>`;
	setTimeout(()=>{
		checkContainer.innerHTML = "";
		residuos.forEach(re => {
			
			checkContainer.innerHTML += `
			<p>
				<label>
					<input name="group1" class="residuos" type="radio" value="${re.id}" />
					<span>${re.nombre}</span>
				</label>
			</p>
			`;
		});	
	},3000);
	
}

function agregarDeposito(e){
	e.preventDefault();
	const inputCantidad = document.querySelector("#cantidad");
	let cantidad = Number(inputCantidad.value);
	inputCantidad.value = "";
	let residuoID = Number(document.querySelector('.residuos:checked').value);
	if(residuoID === 0 || cantidad === 0){
		return;
	} else {
		let avanzar = true;
		const ul = document.querySelector("#registroResiduos");
		ul.childNodes.forEach(lista =>{
			lista.childNodes.forEach(input => {
				if(input.value == residuoID){
					avanzar = false;
				}
			});
		});
		if(avanzar){
			let nombre = document.querySelector('.residuos:checked + span').innerText;
			registroResiduos.innerHTML += `
			<li class="collection-item"><div><b>${nombre}</b> - Cantidad: ${cantidad}<a style='cursor:pointer;' class="secondary-content"><i class="material-icons red-text eliminar-deposito">delete</i></a></div>
			<input type="hidden" value="${residuoID}" name="residuo"><input type="hidden" value="${cantidad}" name="cantidad"></li>
		`;
		}
	}
}

function borrarDeposito(e){
	e.preventDefault();
	if(e.target.classList.contains('eliminar-deposito')){
		e.target.parentElement.parentElement.parentElement.remove();
    } 
}


