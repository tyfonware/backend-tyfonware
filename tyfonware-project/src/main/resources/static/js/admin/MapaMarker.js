class MapaMarker extends Mapa{
    constructor() {
    	super();
    }
    inicializarMapa(data) {
    	
    	let {latitud, longitud} = data;
    	
    	
    	
        this.latLng = { lat: latitud, lng: longitud };
        

        this.mapa = new google.maps.Map(document.getElementById('mapa'), {
            center: this.latLng,
            zoom: 17,
            streetViewControl: false
            // disableDefaultUI: true
        });

        this.marker = new google.maps.Marker({
            map: this.mapa,
            draggable: true,
            position: this.latLng
        });
        
        this.marker.addListener('click', this.toggleBounce);
      
        this.marker.addListener('dragend', function (e) {
        	console.log(e);
        	 document.querySelector("#lat").value = this.getPosition().lat()
        	 document.querySelector("#lng").value = this.getPosition().lng()
        	 
        	 let geocoder = new google.maps.Geocoder();
        	 geocoder.geocode( { latLng: e.latLng }, function ( result, status ) {
        		 if ( 'OK' === status ) {  
        			 let address = result[0].formatted_address;
        			 let input = document.querySelector("#calle");
        			 input.value = address;
        		 } else{
        			 
        		 }
        	 });
   	 
        });
    } 
    toggleBounce() {
        if (marker.getAnimation() !== null) {
            this.marker.setAnimation(null);
        } else {
            this.marker.setAnimation(google.maps.Animation.BOUNCE);
        }
    }
}

