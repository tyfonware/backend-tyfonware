
class MapaRuta extends Mapa{
	constructor() {
    	super();
    }
	inicializarMapa(data) {
    	let {latitud, longitud} = data;
        this.latLng = { lat: latitud, lng: longitud };
        this.mapa = new google.maps.Map(document.getElementById('mapa'), {
            center: this.latLng,
            zoom: 17
        });
        this.directionsService = new google.maps.DirectionsService();
        this.directionsDisplay = new google.maps.DirectionsRenderer();
        this.directionsDisplay.setMap(this.mapa);
    }
    obtenerDepositos(){
    	this.api.obtenerDepositos()
        .then(data =>  {
             const resultado = data;
             this.calcularRuta(this.directionsService, this.directionsDisplay,data)
        } )
    }
    calcularRuta(directionsService, directionsDisplay,data) {
        let waypts = [];
        
        let depositos = [];
        data.depositos.forEach(deposito => {
        	if(deposito.estatus == 0){
        		//:(
        	} else {
        		depositos.push(deposito);
        		waypts.push({
            		location: new google.maps.LatLng(deposito.casa.latitud,deposito.casa.longitud),
                    stopover: true
            	});
        	}
        });
        
        depositos.forEach(deposito => {
        	
        });
        this.directionsService.route({
          origin: this.latLng,
          destination: this.latLng,
          waypoints: waypts,
          optimizeWaypoints: true,
          travelMode: 'DRIVING'
        }, (response, status) => {
          if (status === 'OK') {
        	let letras = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
            this.directionsDisplay.setDirections(response);
            let route = response.routes[0];
            let summaryPanel = document.getElementById('direcciones');
            summaryPanel.innerHTML = '';
            for (let i = 0; i < route.legs.length; i++) {
                let routeSegment = i + 1;
                let registros = '';
                if(depositos !== null){
                	if(depositos.length > (i)){
                		registros += '<h5>Residuos</h5>'
                		registros += '<ul class="collection">';
                		depositos[i].registroResiduos.forEach(registro => {
                			registros += `
                			<li class="collection-item ">
                				<p><b>Residuo:</b> ${registro.residuo.nombre}</p>
                				<p><b>Cantidad:</b> ${registro.cantidad}</p>
                				<hr>
                			</li>`
                		});
                		registros += '</ul>';
                	}
                }
                let html = '';
                html += `
                <li>
                	<div class="collapsible-header"><i class="material-icons">directions</i>Segmento ${routeSegment} [${letras[i]} - ${letras[i+1]}]</div>
                	<div class="collapsible-body"><span>
                		<p><b>De:</b> ${route.legs[i].start_address}</p>
                		<p><b>A:</b> ${route.legs[i].end_address}</p>
                		<p><b>Distancia:</b> ${route.legs[i].distance.text}</p>
                		${registros}
                		</span></div>
                </li>
                `;
                summaryPanel.innerHTML += `${html}`;
                
              }
          } else {
            window.alert('Directions request failed due to ' + status);
          }
        });
      }
   
}