let instance;
let elem;
const listaCasas = document.querySelector('.otros');

document.addEventListener('DOMContentLoaded', function() {
	elem = document.querySelector(".autocomplete");
	instance = M.Autocomplete.getInstance(elem);
	elem.addEventListener('keyup',mostrarDatos);
	elem.addEventListener('keydown',agregarCasaEnter);

});

listaCasas.addEventListener('click',eliminarCasa);

function mostrarDatos(e) {
	let term = e.target.value;
	
	fetch('/admin/propietarios/mostrar-casas/'+term)
    .then(function(response) {
        return response.json();
    })
    .then(function(data) {
        mostrarElementos(data);
    })
    .catch(function(err) {
        console.error(err);
    });
	
}

function mostrarElementos(data){
	datos = data;
	let direcciones = {};
	let instancia = M.Autocomplete.getInstance(document.querySelector(".autocomplete"));
	for(i = 0; i < datos.length;i++){
		direcciones[`${datos[i].direccion}`] = null;
	}
	instancia.updateData(direcciones);
}

// Agregar casas
const btnAgregar = document.querySelector("#agregar");

btnAgregar.addEventListener('click', agregarCasa);

function agregarCasaEnter(e){
	
	if (e.key === "Enter") {
		e.preventDefault();
        agregarCasa(e);
    }
}

function agregarCasa(e){
	const term = document.querySelector("#autocomplete-input");
	fetch('/admin/propietarios/mostrar-casas/'+term.value)
    .then(function(response) {
        return response.json();
    })
    .then(function(data) {
        agregarInput(data,term);
    })
    .catch(function(err) {
        console.error(err);
    });
}

function agregarInput(data,term) {
	const datos = data;
	const ul = document.querySelector(".otros");
	
	let id = datos[0].id;
	let avanzar = true;
	
	ul.childNodes.forEach(lista =>{
		lista.childNodes.forEach(input => {
			if(input.value == id){
				avanzar = false;
			}
		});
	});
	if(avanzar){
		let input = document.createElement("input");

		input.type = "hidden";
		input.name = "casas";
		input.value = id;
		
		let li = document.createElement("li");
		li.innerHTML =`<div>${datos[0].direccion}<a class="secondary-content eliminar-casa"><i class="eliminar-casa material-icons black-text">delete</i></a></div>`;
		li.classList.add("collection-item");
		
		li.appendChild(input);
		ul.appendChild(li);
		term.value = ""
	}
	
}

const formulario = document.querySelector("#formulario");
formulario.addEventListener('submit', function(e) {
	const otros = document.querySelector(".otros")
	return;
});

function eliminarCasa(e){
	e.preventDefault();
	if(e.target.classList.contains('eliminar-casa')){
		e.target.parentElement.parentElement.parentElement.remove();
    } 
	
}