
class MapaCasas extends Mapa{
	constructor() {
    	super();
    }
	inicializarMapa(data) {
    	let {latitud, longitud} = data;
        this.latLng = { lat: latitud, lng: longitud };
        this.mapa = new google.maps.Map(document.getElementById('mapa'), {
            center: this.latLng,
            zoom: 17,
            styles: [{
            	 "featureType": "poi.business",
            	 "stylers": [{"visibility": "off"}]
            	},
            	{
            	 "featureType": "transit",
            	 "elementType": "labels.icon",
            	 "stylers": [{"visibility": "off"}]
            	}]
        });
        this.obtenerCasas();
    }
    obtenerCasas(){
    	this.api.obtenerCasas()
        .then(data =>  {
             const resultado = data;
             this.mostrarMapa(resultado);
        } )
    }
    mostrarMapa(datos){
    	 // Almacena InfoWindow Activo
        let infoWindowActivo;

        // Recorrer establecimientos
        datos.forEach(dato => {
        	
             // Destucturing 
             let {latitud, longitud, direccion,id} = dato;

             
             
             // Crear objeto con latitud y longitud
             let latLng = {
                  lat: Number( latitud ), 
                  lng: Number( longitud)
             }

             // Agregar el Pin
             let marker = new google.maps.Marker({
                  position: latLng,
                  map: this.mapa
             });
             // Crear infoWindow

             let infoWindow = this.crearInfoWindow(direccion,id);

             // Mostrar InfoWindow al hacer click
             marker.addListener('click', () => {
                  // Cerrar infoWindowActivo
                  if(infoWindowActivo) {
                       infoWindowActivo.close();
                  }

                  // Mostrarlo
                  infoWindow.open(this.mapa, marker);

                  // Asignarlo a activo
                  infoWindowActivo = infoWindow;
             })

        })
    }
    crearInfoWindow(calle,id) {
    	console.log(calle);
        let contenido = `
             <p><b>Domicilio:</b> <a href='/admin/casas/casa/${id}'>${calle}</a> </p>
        `;
        let infoWindow = new google.maps.InfoWindow({
             content: contenido
        });
        return infoWindow;
   }
}