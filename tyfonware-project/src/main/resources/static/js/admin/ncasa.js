const ui = new MapaMarker();
const form = document.querySelector("#formulario");

form.addEventListener("submit",validar);

function validar(e){
	latitud = e.target.lat.value;
	longitud = e.target.lng.value;
	
	if(latitud === "" || longitud === ""){
		e.preventDefault();
		ui.centrar();
	}
}
