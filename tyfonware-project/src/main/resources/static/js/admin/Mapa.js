class Mapa{
	constructor() {
    	this.api = new API();
    	this.obtenerCoordenadas();
    }
    obtenerCoordenadas(){
    	this.api.obtenerCoordenadas()
        .then(data =>  {
             const resultado = data;
             this.inicializarMapa(resultado);
        } )
    }
    inicializarMapa(data) {
    	let {latitud, longitud} = data;
        this.latLng = { lat: latitud, lng: longitud };
        this.mapa = new google.maps.Map(document.getElementById('mapa'), {
            center: this.latLng,
            zoom: 17,
            styles: [{
           	 "featureType": "poi.business",
           	 "stylers": [{"visibility": "off"}]
           	},
           	{
           	 "featureType": "transit",
           	 "elementType": "labels.icon",
           	 "stylers": [{"visibility": "off"}]
           	}]
            //streetViewControl: false
            // disableDefaultUI: true
        });
        
    }
    centrar(){
        let centro = new google.maps.LatLng(this.latLng);
        this.mapa.panTo(centro);
    }
}