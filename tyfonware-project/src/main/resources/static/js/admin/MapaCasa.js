class MapaCasa extends Mapa{
    constructor() {
    	super();
    }
    inicializarMapa(data) {
    	
    	let latitud = Number(document.querySelector('#latitud').value);
    	let longitud = Number(document.querySelector('#longitud').value);
    	
        this.latLng = { lat: latitud, lng: longitud };
        
        this.mapa = new google.maps.Map(document.getElementById('mapa'), {
            center: this.latLng,
            zoom: 17,
            streetViewControl: false,
            disableDefaultUI: true
        });
        
        
        
        this.marker = new google.maps.Marker({
            map: this.mapa,
            position: this.latLng
        });
    } 
}
